-- name: Rico's Scrapyard
-- author: Andrew Pamment
-- version: 0.2


while true do
	fh_clr_screen();
    fh_send_file("scrapyard.ans");
    
    local aval, astr = fh_get_armor();
    local wval, wstr = fh_get_weapon();

    if aval > 0 then
        fh_gotoxy(42, 12);
        fh_print("`bright white`(`bright green`A`bright white`) Sell your " .. astr);
    end
    if wval > 0 then
        fh_gotoxy(42, 13);
        fh_print("`bright white`(`bright green`W`bright white`) Sell your " .. wstr);
    end

    if aval == 0 and wval == 0 then
        fh_gotoxy(42, 12);
        fh_print("`bright white`You have nothing to sell!");
    end
    fh_gotoxy(42, 15);
    fh_print("`bright white`(`bright green`R`bright white`) Return to Town");

    fh_gotoxy(2, 23);
    fh_print("`bright white`Rico looks you over, want to sell something? ");

    local inp = fh_getkey();

    if inp == 'a' or inp == 'A' then
        if (aval == 0) then
            fh_print("\r\n\r\n`bright white`Huh?!\r\n");
        else
            fh_print("\r\n\r\n`bright white`I'll give you `bright yellow`" .. math.floor(aval * 1000 / 3) .. "`bright white` for that junky armor. (Y/ N) ? ");
            local yn = fh_getkey();
            if yn == 'y' or yn == 'Y' then
                fh_set_armor(0);
                local gold = fh_get_gold();
                gold = gold + math.floor(aval * 1000 / 3);
                fh_set_gold(gold);
                fh_print("\r\n\r\n`bright white`Done! You feel a little chilly.\r\n\r\n");
            end
        end
    elseif inp == 'w' or inp == 'W' then
        if (wval == 0) then
            fh_print("\r\n\r\n`bright white`Huh?!\r\n");
        else
            fh_print("\r\n\r\n`bright white`I'll give you `bright yellow`" .. math.floor(wval * 1000 / 3) .. "`bright white` for that junky weapon. (Y/ N) ? ");
            local yn = fh_getkey();
            if yn == 'y' or yn == 'Y' then
                fh_set_weapon(0);
                local gold = fh_get_gold();
                gold = gold + math.floor(wval * 1000 / 3);
                fh_set_gold(gold);
                fh_print("\r\n\r\n`bright white`Done! You feel a little vulnerable.\r\n\r\n");
            end
        end
    else
        return
    end
end