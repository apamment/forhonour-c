-- name: Experience Setter
-- author: Andrew Pamment
-- version: 0.1

while true do
	fh_print("(G) Give Experience\r\n");
	fh_print("(R) Return to Town\r\n");
	local inp = fh_getkey();

	if inp == "g" or inp == "G" then
		fh_print("How much experience? ");
		local exp = tonumber(fh_getstring(5));

		fh_increase_exp(exp);
		return;
	elseif inp == "r" or inp == "R" then
		return;
	end
end