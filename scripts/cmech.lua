-- name: Cmech's Bar and Grill
-- author: Andrew Pamment
-- version: 0.2

math.randomseed(os.time());
while true do
	fh_clr_screen();
	fh_send_file("cmech.ans");

	local inp = fh_getkey();
	local gold = fh_get_gold();

	if inp == "s" or inp == "S" then
		if gold >= 100 then
			local chance = math.random(2);
			local luck = fh_get_luck();
			
			gold = gold - 100;
			fh_set_gold(gold);
			
			if chance == 1 then
				fh_print("`bright white`\r\n\r\nAs you bite into your delicious steak, you feel a little luckier!`white`\r\n");
				luck = luck + 5;
				fh_set_luck(luck);
			else
				fh_print("`bright white`\r\n\r\nYour steak is delicious, and you wonder about ordering another...`white`\r\n");
			end
			fh_print("`bright green`Press a key...`white`");
			fh_getkey();
			fh_print("\r\n\r\n");
		else
			fh_print("`bright white`\r\n\r\n*chuckle* you can't afford that friend, perhaps another time.`white`\r\n");
			fh_print("`bright green`Press a key...`white`");
			fh_getkey();
			fh_print("\r\n\r\n");
		end
	elseif inp == "b" or inp == "B" then
		if gold >= 100 then
			local chance = math.random(2);
			local charm = fh_get_charm();
			
			gold = gold - 100;
			fh_set_gold(gold);
			
			if chance == 1 then
				fh_print("`bright white`\r\n\r\nYou sip your brew, and feel just a little more charming!`white`\r\n");
				charm = charm + 5;
				fh_set_charm(charm);
			else
				fh_print("`bright white`\r\n\r\nYou sip your brew, and you wonder about ordering another...`white`\r\n");
			end
			fh_print("`bright green`Press a key...`white`");
			fh_getkey();
			fh_print("\r\n\r\n");
		else
			fh_print("`bright white`\r\n\r\n*chuckle* you can't afford that friend, perhaps another time.`white`\r\n");
			fh_print("`bright green`Press a key...`white`");
			fh_getkey();
			fh_print("\r\n\r\n");
		end
	elseif inp == "r" or inp == "R" then
		return
	end
	
	fh_print("\r\n\r\n");
end
