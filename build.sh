#!/bin/bash -e

if [ ! -e MagiDoor ]; then
    git clone https://gitlab.com/magickabbs/MagiDoor.git
fi

cd MagiDoor
if [ -e build ]; then
    rm -rf build
fi
mkdir build
cd build
cmake ..
make
cd ../..

gcc -c main.c -o main.o -I./MagiDoor/ 
gcc -c otherplaces.c -o otherplaces.o `pkg-config --cflags lua5.3` -I./MagiDoor/
gcc -c interbbs2.c -o interbbs2.o 
gcc -c inih/ini.c -o inih/ini.o 
gcc -o ForHonour main.o interbbs2.o otherplaces.o inih/ini.o MagiDoor/build/libmdoor.a -lsqlite3 `pkg-config --libs lua5.3` -lm
rm -rf MagiDoor

gcc -c report.c -o report.o
gcc -o report report.o interbbs2.o -lsqlite3