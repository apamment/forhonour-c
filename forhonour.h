#ifndef __FORHONOUR_H__
#define __FORHONOUR_H__

#include <time.h>

struct user_info {
	char name[32];
	int fights_left;
	time_t last_played;
	int level;
	int experience;
	int userClass;
	int luck;
	int charm;
	int strength;
	int dexterity;
	int wisdom;
	int constitution;
	int health;
	int gold;
	int weaponvalue;
	int armorvalue;
	int goldInBank;
	int stayingAtInn;
	int bribeAmount;
	int challengedMasterToday;
	int learnedSkill1;
	int learnedSkill2;
	int learnedSkill3;
	int skillPoints;
	int flirtedWithAbbey;
	int listenedToOldMan;
	int gems;
	int enchant;
	int potions;
	int treasuremap;
}__attribute__((packed));

struct monster_info {
	int level;
	int toughness;
	char name[32];
	char attack1[64];
	char attack2[64];
	int health;
};

struct bar_talk {
	char line[80];
	char name[32];
};

extern void save_player();
extern void add_news_item(char *item);
extern char *get_weapon_name();
extern char *get_armor_name();

extern int load_other_places();
extern void other_places_menu();
extern void other_places_versions();
#endif