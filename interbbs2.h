#ifndef __IBBS_2
#define __IBBS_2

#define FILEEXT "FHR"
#define VERSION "00005"
#define BBSCFG FILEEXT "-IBBS.CFG"

typedef enum {
  eSuccess,
  eForwarded,
  eNoMoreMessages,
  eGeneralFailure,
  eBadParameter,
  eNoMemory,
  eMissingDir,
  eFileOpenError,
  eWrongLeague,
  eWrongGameID
} tIBResult;

#ifndef tBool
typedef int tBool;
#endif
#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

#define SYSTEM_NAME_CHARS 39

#ifdef _MSC_VER
#define PATH_SEP "\\"
#else
#define PATH_SEP "/"
#endif

typedef struct {
  uint32_t nodeNumber;
  char filebox[PATH_MAX + 1];
  char name[SYSTEM_NAME_CHARS + 1];
} tOtherNode;

typedef struct {
  uint32_t league;
  uint32_t game_id;
  char defaultFilebox[PATH_MAX + 1];
  tOtherNode *myNode;
  tOtherNode **otherNodes;
  int otherNodeCount;
} tIBInfo;

void IBSetLogger(void (*l)(char *fmt, ...));
tIBResult IBSend(tIBInfo *pInfo, int pszDestNode, void *pBuffer,
                 uint32_t nBufferSize);
tIBResult IBSendAll(tIBInfo *pInfo, void *pBuffer, uint32_t nBufferSize);
tIBResult IBGet(tIBInfo *pInfo, void *pBuffer, uint32_t nMaxBufferSize);
tIBResult IBReadConfig(tIBInfo *pInfo, char *pszConfigFile);

#endif
