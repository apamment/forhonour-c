/*
    For Honour
    Copyright (C) 2017  Andrew Pamment

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#define VERSION_MAJOR 1
#define VERSION_MINOR 91

#ifndef VERSION_TYPE
#define VERSION_TYPE "dev"
#endif

#include <MagiDoor.h>
#include <ctype.h>
#include <fcntl.h>
#include <limits.h>
#include <sqlite3.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include "forhonour.h"
#include "inih/ini.h"
#include "interbbs2.h"

#if defined(_MSC_VER) || defined(WIN32)
#define snprintf _snprintf
#define strcasecmp _stricmp
#include <winsock2.h>
#ifndef _MSC_VER
#define _MSC_VER 1
#endif
#else
#include <arpa/inet.h>
#endif

char *log_path;
char *bad_path;
int delete_bad;
struct user_info info;
int player_idx = -1;
int full = 0;
tIBInfo InterBBSInfo;
int interBBSMode;

typedef struct ibbsscore {
  char player_name[32];
  char bbs_name[40];
  int score;
  int experience;
} ibbsscores_t;

typedef struct ibbsmsg {
  int32_t type;
  uint32_t from;
  uint32_t fights_per_day;
  char player_name[32];
  uint32_t score;
  uint32_t experience;
  uint32_t created;
} __attribute__((packed)) ibbsmsg_t;

int ini_max_fights = 10;

void msg2ne(ibbsmsg_t *msg) {
  msg->type = htonl(msg->type);
  msg->from = htonl(msg->from);
  msg->fights_per_day = htonl(msg->fights_per_day);
  msg->score = htonl(msg->score);
  msg->experience = htonl(msg->experience);
  msg->created = htonl(msg->created);
}

void msg2he(ibbsmsg_t *msg) {
  msg->type = ntohl(msg->type);
  msg->from = ntohl(msg->from);
  msg->fights_per_day = ntohl(msg->fights_per_day);
  msg->score = ntohl(msg->score);
  msg->experience = ntohl(msg->experience);
  msg->created = ntohl(msg->created);
}


void add_link(int link, const char* linkname) {
	FILE* fptr;
	fptr = fopen(FILEEXT "-IBBS.CFG", "a");
	if (!fptr)
	{
		dolog("Unable to open " FILEEXT "-IBBS.CFG");
		return;
	}
	fprintf(fptr, "\nLinkNodeNumber %d\n", link);
	fprintf(fptr, "LinkName %s\n", linkname);
	fclose(fptr);

}

void remove_link(int link) {
	FILE *fptr;
	FILE *fptr2;
	char message[256];
	char message2[256];
	int stage = 0;

	dolog("Removing link %d from game", link);

	fptr = fopen(FILEEXT "-IBBS.CFG", "r");
	if (!fptr)
	{
		dolog("Unable to open " FILEEXT "-IBBS.CFG");
		return;
	}

	fptr2 = fopen(FILEEXT "-IBBS.CFG.BAK", "w");
	if (!fptr2)
	{
		dolog("Unable to open " FILEEXT "-IBBS.CFG.BAK");
		return;
	}
	fgets(message, 256, fptr);
	while (!feof(fptr))
	{
		fputs(message, fptr2);
		fgets(message, 256, fptr);
	}
	fclose(fptr2);
	fclose(fptr);
	fptr = fopen(FILEEXT "-IBBS.CFG.BAK", "r");
	if (!fptr)
	{
		dolog("Unable to open " FILEEXT "-IBBS.CFG.BAK");
		return;
	}

	fptr2 = fopen(FILEEXT "-IBBS.CFG", "w");
	if (!fptr2)
	{
		dolog("Unable to open " FILEEXT "-IBBS.CFG");
		return;
	}

	sprintf(message2, "LinkNodeNumber %d", link);

	fgets(message, 256, fptr);
	while (!feof(fptr))
	{
		if (strncasecmp(message, message2, strlen(message2)) == 0)
		{
			stage = 1;
		}
		else if (strncasecmp(message, "LinkNodeNumber", 14) == 0 && stage == 1)
		{
			stage = 0;
		}
		if (stage == 0 || message[0] == ';')
		{
			fputs(message, fptr2);
		}
		else {
			fprintf(fptr2, "; DELETED %s", message);
		}
		fgets(message, 256, fptr);
	}
	fclose(fptr2);
	fclose(fptr);
	unlink(FILEEXT "-IBBS.CFG.BAK");
}

void dolog(char *fmt, ...) {
  char buffer[PATH_MAX];
  struct tm *time_now;
  time_t timen;
  FILE *logfptr;

  timen = time(NULL);

  time_now = localtime(&timen);
  if (log_path != NULL) {
    snprintf(buffer, PATH_MAX, "%s%s%04d%02d%02d.log", log_path, PATH_SEP,
             time_now->tm_year + 1900, time_now->tm_mon + 1, time_now->tm_mday);
  } else {
    snprintf(buffer, PATH_MAX, "%04d%02d%02d.log", time_now->tm_year + 1900,
             time_now->tm_mon + 1, time_now->tm_mday);
  }
  logfptr = fopen(buffer, "a");
  if (!logfptr) {
    return;
  }
  va_list ap;
  va_start(ap, fmt);
  vsnprintf(buffer, 512, fmt, ap);
  va_end(ap);

  fprintf(logfptr, "%02d:%02d:%02d %s\n", time_now->tm_hour, time_now->tm_min,
          time_now->tm_sec, buffer);

  fclose(logfptr);
}

static int handler(void *user, const char *section, const char *name,
                   const char *value) {
  if (strcasecmp(section, "main") == 0) {
    if (strcasecmp(name, "log path") == 0) {
      log_path = strdup(value);
#if defined(_MSC_VER) || defined(WIN32)
      if (log_path[strlen(log_path) - 1] == '\\') {
#else
      if (log_path[strlen(log_path) - 1] == '/') {
#endif
        log_path[strlen(log_path) - 1] = '\0';
      }
    } else if (strcasecmp(name, "bad path") == 0) {
      bad_path = strdup(value);
#if defined(_MSC_VER) || defined(WIN32)
      if (bad_path[strlen(bad_path) - 1] == '\\') {
#else
      if (bad_path[strlen(bad_path) - 1] == '/') {
#endif
        bad_path[strlen(bad_path) - 1] = '\0';
      }
    } else if (strcasecmp(name, "delete bad") == 0) {
      if (strcasecmp(value, "true") == 0) {
        delete_bad = 1;
      } else {
        delete_bad = 0;
      }
    } else if (strcasecmp(name, "max fights") == 0) {
      ini_max_fights = atoi(value);
    }
  } else if (strcasecmp(section, "interbbs") == 0) {
    if (strcasecmp(name, "enabled") == 0) {
      if (strcasecmp(value, "true") == 0) {
        interBBSMode = 1;
      } else {
        interBBSMode = 0;
      }
    } else if (strcasecmp(name, "system name") == 0) {
      strncpy(InterBBSInfo.myNode->name, value, SYSTEM_NAME_CHARS);
      InterBBSInfo.myNode->name[SYSTEM_NAME_CHARS] = '\0';
    } else if (strcasecmp(name, "league number") == 0) {
      InterBBSInfo.league = atoi(value);
    } else if (strcasecmp(name, "node number") == 0) {
      InterBBSInfo.myNode->nodeNumber = atoi(value);
    } else if (strcasecmp(name, "file inbox") == 0) {
      strncpy(InterBBSInfo.myNode->filebox, value, PATH_MAX);
      InterBBSInfo.myNode->filebox[PATH_MAX] = '\0';
    } else if (strcasecmp(name, "default outbox") == 0) {
      strncpy(InterBBSInfo.defaultFilebox, value, PATH_MAX);
      InterBBSInfo.defaultFilebox[PATH_MAX] = '\0';
    }
  }
  return 1;
}

void print_bar(int health, int max_health, const char *color) {
  int bars ;
  int i;

  if (health > max_health) {
    bars = 20;
  } else {
    bars = (int)(((float)health / (float)max_health) * 20.f);
  }

  md_printf("`bright black`[");

  if (bars == 0 && health > 0) {
    bars = 1;
  }

  if (bars > 0) {
    md_printf("`%s`", color);
  }
  for (i=0;i<bars;i++) {
    md_printf("#");
  }
  md_printf("`bright black`");
  for (i=bars;i<20;i++) {
    md_printf("-");
  }
  md_printf("] (`%s`%d`bright black`)`white`", color, health);
 
}

void copy_expand(FILE *src, FILE *des) {
  // copies from one file to another, expanding out |V pipe code
  // closes src file.
  int gotpipe = 0;
  char c;
  
  c = fgetc(src);
  while (!feof(src)) {
    if (gotpipe == 1) {
      if (c == 'V') {
	fprintf(des, "v%d.%d-%s", VERSION_MAJOR, VERSION_MINOR,
		VERSION_TYPE);
      } else {
	fprintf(des, "|%c", c);
      }
      gotpipe = 0;
    } else {
      if (c == '|') {
	gotpipe = 1;
      } else {
	fputc(c, des);
      }
    }
    c = fgetc(src);
  }
  fclose(src);
}

void build_interbbs_scorefile() {

  FILE *fptr, *fptr2;
  sqlite3 *db;
  char sqlbuffer[256];
  int rc;
  sqlite3_stmt *stmt;
  ibbsscores_t **scores;
  int player_count;
  ibbsscores_t *ptr;
  int i;
  int j;

  scores = NULL;
  player_count = 0;

  struct user_info inf;
  fptr = fopen("players.dat", "rb");
  if (fptr) {
    while (fread(&inf, sizeof(struct user_info), 1, fptr) == 1) {
      if (scores == NULL) {
        scores = (ibbsscores_t **)malloc(sizeof(ibbsscores_t *));
      } else {
        scores = (ibbsscores_t **)realloc(scores, sizeof(ibbsscores_t *) *
                                                      (player_count + 1));
      }

      if (scores == NULL) {
        fclose(fptr);
        dolog("OOM");
        exit(-1);
      }

      scores[player_count] = (ibbsscores_t *)malloc(sizeof(ibbsscores_t));
      strncpy(scores[player_count]->player_name, inf.name, 32);
      strcpy(scores[player_count]->bbs_name, InterBBSInfo.myNode->name);
      scores[player_count]->score = inf.level;
      scores[player_count]->experience = inf.experience;
      player_count++;
    }
    fclose(fptr);
  }

  rc = sqlite3_open("interbbs.db3", &db);
  if (rc) {
    // Error opening the database
    dolog("Error opening interbbs database: %s", sqlite3_errmsg(db));
    sqlite3_close(db);
    exit(1);
  }
  sqlite3_busy_timeout(db, 5000);

  snprintf(sqlbuffer, 256,
           "SELECT gamename,address,score,experience FROM scores;");
  sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
  while (sqlite3_step(stmt) == SQLITE_ROW) {
    if (scores == NULL) {
      scores = (ibbsscores_t **)malloc(sizeof(ibbsscores_t *));
    } else {
      scores = (ibbsscores_t **)realloc(scores, sizeof(ibbsscores_t *) *
                                                    (player_count + 1));
    }

    if (scores == NULL) {
      dolog("OOM");
      exit(-1);
    }

    scores[player_count] = (ibbsscores_t *)malloc(sizeof(ibbsscores_t));
    strncpy(scores[player_count]->player_name,
            (char *)sqlite3_column_text(stmt, 0), 17);

    for (i = 0; i < InterBBSInfo.otherNodeCount; i++) {
      if (sqlite3_column_int(stmt, 1) ==
          InterBBSInfo.otherNodes[i]->nodeNumber) {
        strncpy(scores[player_count]->bbs_name,
                InterBBSInfo.otherNodes[i]->name, 40);
        break;
      }
    }

    scores[player_count]->score = sqlite3_column_int(stmt, 2);
    scores[player_count]->experience = sqlite3_column_int(stmt, 3);
    player_count++;
  }
  sqlite3_finalize(stmt);
  sqlite3_close(db);

  for (i = 0; i < player_count - 1; i++) {
    for (j = 0; j < player_count - i - 1; j++) {
      if (scores[j]->experience < scores[j + 1]->experience) {
        ptr = scores[j];
        scores[j] = scores[j + 1];
        scores[j + 1] = ptr;
      }
    }
  }
  fptr = fopen("ibbs_scores.ans", "w");
  if (fptr) {
    fptr2 = fopen("ibbs_score_header.ans", "r");
    if (fptr2) {
      copy_expand(fptr2, fptr);
    }
    for (i = 0; i < player_count; i++) {
#if defined(_MSC_VER) || defined(WIN32)
      fprintf(fptr, "\x1b[0m %-31.31s %-31.31s %6u (%d)\n",
              scores[i]->player_name, scores[i]->bbs_name,
              scores[i]->experience, scores[i]->score);
#else
      fprintf(fptr, "\x1b[0m %-31.31s %-31.31s %6u (%d)\r\n",
              scores[i]->player_name, scores[i]->bbs_name,
              scores[i]->experience, scores[i]->score);
#endif
    }

    fptr2 = fopen("ibbs_score_footer.ans", "r");
    if (fptr2) {
      copy_expand(fptr2, fptr);
    }
    fclose(fptr);
  }
  fptr = fopen("ibbs_scores.asc", "w");

  if (fptr) {
    fptr2 = fopen("ibbs_score_header.asc", "r");
    if (fptr2) {
      copy_expand(fptr2, fptr);
    }
    for (i = 0; i < player_count; i++) {
#if defined(_MSC_VER) || defined(WIN32)
      fprintf(fptr, " %-31.31s %-31.31s %6u (%d)\n", scores[i]->player_name,
              scores[i]->bbs_name, scores[i]->experience, scores[i]->score);
#else
      fprintf(fptr, " %-31.31s %-31.31s %6u (%d)\r\n", scores[i]->player_name,
              scores[i]->bbs_name, scores[i]->experience, scores[i]->score);
#endif
    }
    fptr2 = fopen("ibbs_score_footer.asc", "r");
    if (fptr2) {
      copy_expand(fptr2, fptr);
    }
    fclose(fptr);
  }

  for (i = 0; i < player_count; i++) {
    free(scores[i]);
  }
  free(scores);
}

void perform_maintenance() {
  ibbsmsg_t msg;
  ibbsmsg_t outboundmsg;
  int i;
  int id;
  int rc;
  sqlite3_stmt *stmt;
  sqlite3 *db;
  char sqlbuffer[256];
  int k;
  time_t last_score;
  tIBResult result;
  char message[256];
  FILE *fptr, *fptr2;
  uint32_t newgameid;
  uint32_t newnodenum;
  int reset = 0;
  time_t last_recon;
  struct user_info inf;
  dolog("Performing maintenance...");

  // parse all incoming messages
  i = 0;
  k = 0;
  if (interBBSMode == 1) {
    while (1) {
      result = IBGet(&InterBBSInfo, &msg, sizeof(ibbsmsg_t));

      if (result == eSuccess) {
        msg2he(&msg);

        if (msg.fights_per_day != ini_max_fights) {
          if (msg.from == 1) {
            dolog("Your max fights settings are wrong for the league! Should be %d", msg.fights_per_day);
          } else {
            dolog("Got packet with incorrect max fights. Ignoring. (From %d)", msg.from);
          }
          continue;
        }

        memset(&outboundmsg, 0, sizeof(ibbsmsg_t));

        switch (msg.type) {
        case 1:
          // add score to database
          dolog("Got score file packet for player %s", msg.player_name);
          rc = sqlite3_open("interbbs.db3", &db);
          if (rc) {
            // Error opening the database
            dolog("Error opening interbbs database: %s", sqlite3_errmsg(db));
            sqlite3_close(db);
            return;
          }
          sqlite3_busy_timeout(db, 5000);
          snprintf(
              sqlbuffer, 256,
              "SELECT id, last FROM scores WHERE gamename=? and address=?");
          sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
          sqlite3_bind_text(stmt, 1, msg.player_name,
                            strlen(msg.player_name), SQLITE_STATIC);
          sqlite3_bind_int(stmt, 2, msg.from);

          rc = sqlite3_step(stmt);

          if (rc == SQLITE_ROW) {
            id = sqlite3_column_int(stmt, 0);
            last_score = sqlite3_column_int(stmt, 1);
            sqlite3_finalize(stmt);
            if (last_score < msg.created) {
              snprintf(
                  sqlbuffer, 256,
                  "UPDATE scores SET score=?, experience=?, last=? WHERE id=?");
              sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt,
                                 NULL);
              sqlite3_bind_int(stmt, 1, msg.score);
              sqlite3_bind_int(stmt, 2, msg.experience);
              sqlite3_bind_int(stmt, 3, msg.created);
              sqlite3_bind_int(stmt, 4, id);
              sqlite3_step(stmt);
              sqlite3_finalize(stmt);
            }
          } else {
            sqlite3_finalize(stmt);
            snprintf(sqlbuffer, 256,
                     "INSERT INTO scores (address, gamename, score, "
                     "experience, last) VALUES(?, ?, ?, ?, ?)");
            sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt,
                               NULL);
            sqlite3_bind_int(stmt, 1, msg.from);
            sqlite3_bind_text(stmt, 2, msg.player_name,
                              strlen(msg.player_name), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 3, msg.score);
            sqlite3_bind_int(stmt, 4, msg.experience);
            sqlite3_bind_int(stmt, 5, msg.created);
            sqlite3_step(stmt);
            sqlite3_finalize(stmt);
          }
          sqlite3_close(db);

          break;
        case 5:
          // new node
          if (msg.from != 1)
          {
            dolog("Received ADD/REMOVE from system not Node 1");
            break;
          }
          if (msg.score == 0)
          {
            dolog("Received ADD Node Packet");
            newnodenum = msg.experience;
            remove_link(newnodenum);
            add_link(newnodenum, msg.player_name);
          }
          else if (msg.score == 1)
          {
            dolog("Received REMOVE Node Packet");
            newnodenum = msg.experience;
            remove_link(newnodenum);
          }
          break;
        case 7:
          if (msg.from == 1) {
            newgameid = atoi(msg.player_name);
            fptr = fopen(BBSCFG, "r");
            if (!fptr) {
              dolog("Unable to open " BBSCFG);
              break;
            }

            fptr2 = fopen(BBSCFG ".BAK", "w");
            if (!fptr2) {
              dolog("Unable to open " BBSCFG ".BAK");
              break;
            }
            fgets(message, 256, fptr);
            while (!feof(fptr)) {
              fputs(message, fptr2);
              fgets(message, 256, fptr);
            }
            fclose(fptr2);
            fclose(fptr);

            fptr = fopen(BBSCFG ".BAK", "r");
            if (!fptr) {
              dolog("Unable to open " BBSCFG ".BAK");
              break;
            }

            fptr2 = fopen(BBSCFG, "w");
            if (!fptr2) {
              dolog("Unable to open " BBSCFG);
              break;
            }

            fgets(message, 256, fptr);
            while (!feof(fptr)) {
              if (strncasecmp(message, "GameID", 6) == 0) {
                fprintf(fptr2, "GameID %d\n", newgameid);
              } else {
                fputs(message, fptr2);
              }
              fgets(message, 256, fptr);
            }
            fclose(fptr2);
            fclose(fptr);
            unlink(BBSCFG ".BAK");

            reset = 1;
          } else {
            dolog("Got game id change from someone not node 1, ignoring");
          }
          break;
        case 12:
          // score recon packet
          // send all score messages

          fptr = fopen("players.dat", "rb");
          if (fptr) {
            while (fread(&inf, sizeof(struct user_info), 1, fptr) == 1) {
              memset(&outboundmsg, 0, sizeof(ibbsmsg_t));
              outboundmsg.type = 1;
              outboundmsg.fights_per_day = ini_max_fights;
              outboundmsg.from = InterBBSInfo.myNode->nodeNumber;
              strcpy(outboundmsg.player_name, inf.name);
              outboundmsg.score = inf.level;
              outboundmsg.experience = inf.experience;
              outboundmsg.created = time(NULL);
              msg2ne(&outboundmsg);
              if (IBSend(&InterBBSInfo, msg.from, &outboundmsg,
                         sizeof(ibbsmsg_t)) != eSuccess) {
                dolog("Unable to write score packet to outbound.");
              }
            }
            fclose(fptr);
          }

          break;
        default:
          dolog("Unknown message type: %d", msg.type);
          break;
        }
        i++;
      } else if (result == eForwarded) {
        k++;
      } else {
        break;
      }
    }
    if (reset == 1) {
      dolog("Got reset message! resetting the game...");
#if defined(_MSC_VER) || defined(WIN32)
      system("reset.bat");
#else
      system("./reset.sh");
#endif
      if (unlink("inuse.flg") != 0) {
        perror("unlink ");
      }

      exit(0);
    }
    dolog("Parsed %d inbound messages; Forwarded %d messages", i, k);

    for (i = 0; i < InterBBSInfo.otherNodeCount; i++) {
      if (InterBBSInfo.otherNodes[i]->nodeNumber ==
          InterBBSInfo.myNode->nodeNumber) {
        continue;
      }
      rc = sqlite3_open("interbbs.db3", &db);
      if (rc) {
        // Error opening the database
        dolog("Error opening interbbs database: %s", sqlite3_errmsg(db));
        sqlite3_close(db);
        exit(1);
      }
      sqlite3_busy_timeout(db, 5000);
      snprintf(sqlbuffer, 256,
               "SELECT * FROM recon WHERE last > ? AND address = ?;");
      sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
      last_recon = time(NULL) - 43200;
      sqlite3_bind_int(stmt, 1, last_recon);
      sqlite3_bind_int(stmt, 2, InterBBSInfo.otherNodes[i]->nodeNumber);
      if (sqlite3_step(stmt) != SQLITE_ROW) {
        sqlite3_finalize(stmt);

        // send recon packet
        dolog("Sending score RECON packet to node %d",
              InterBBSInfo.otherNodes[i]->nodeNumber);
        memset(&outboundmsg, 0, sizeof(ibbsmsg_t));
        outboundmsg.type = 12;
        outboundmsg.fights_per_day = ini_max_fights;
        outboundmsg.from = InterBBSInfo.myNode->nodeNumber;
        outboundmsg.created = time(NULL);
        msg2ne(&outboundmsg);
        if (IBSend(&InterBBSInfo, InterBBSInfo.otherNodes[i]->nodeNumber,
                   &outboundmsg, sizeof(ibbsmsg_t)) != eSuccess) {
          dolog("Unable to write recon packet to outbound.");
        }
        // check if node is in recon list
        snprintf(sqlbuffer, 256, "SELECT * FROM recon WHERE address = ?;");
        sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
        sqlite3_bind_int(stmt, 1, InterBBSInfo.otherNodes[i]->nodeNumber);
        if (sqlite3_step(stmt) == SQLITE_ROW) {
          sqlite3_finalize(stmt);
          snprintf(sqlbuffer, 256,
                   "UPDATE recon SET last = ? WHERE address = ?");
          sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
          last_recon = time(NULL);
          sqlite3_bind_int(stmt, 1, last_recon);
          sqlite3_bind_int(stmt, 2, InterBBSInfo.otherNodes[i]->nodeNumber);
          sqlite3_step(stmt);
        } else {
          sqlite3_finalize(stmt);
          snprintf(sqlbuffer, 256,
                   "INSERT into recon (last, address) VALUES(?, ?)");
          sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
          last_recon = time(NULL);
          sqlite3_bind_int(stmt, 1, last_recon);
          sqlite3_bind_int(stmt, 2, InterBBSInfo.otherNodes[i]->nodeNumber);
          sqlite3_step(stmt);
        }
      }
      sqlite3_finalize(stmt);
      sqlite3_close(db);
    }
    // build global top scores
    build_interbbs_scorefile();
  }
}

char *get_weapon_enchant() {
  switch (info.enchant) {
  default:
    return "";
  case 1:
    return " of Fleetness";
  case 2:
    return " of Sharpness";
  case 3:
    return " of Regeneration";
  case 4:
    return " of The Hoard";
  }
}

char *get_weapon_name() {
  switch (info.weaponvalue) {
  default:
    return "Nothing!";
  case 1:
    return "Large Stick";
  case 5:
    return "Dagger";
  case 7:
    return "Knotty Club";
  case 10:
    return "Rusty Old Sword";
  case 15:
    return "Rapier";
  case 20:
    return "Cutlass";
  case 25:
    return "Short Sword";
  case 30:
    return "Long Sword";
  case 35:
    return "Bastard Sword";
  }
}

char *get_armor_name() {
  switch (info.armorvalue) {
  default:
    return "Nothing!";
  case 1:
    return "Dirty Old Rags";
  case 5:
    return "Patchwork Shirt";
  case 7:
    return "Leather Cuiress";
  case 10:
    return "Rusty Chainmail Shirt";
  case 15:
    return "Bronze Chainmail Shirt";
  case 20:
    return "Iron Chainmail Shirt";
  case 25:
    return "Iron Breastplate";
  case 30:
    return "Steel Breastplate";
  case 35:
    return "Mithril Cuiress";
  }
}

int get_hitval_armor_reduction(int hitval, int armorval) {
  return (int)((float)hitval * ((float)armorval / 100.f));
}

void add_news_item(char *item) {
  char buffer[256];
  FILE *fptr;
  time_t lastnews;
  time_t timenow;
  struct tm today_tm;
  struct tm last_tm;
  struct tm *ptr;

  fptr = fopen("dailynews.dat", "r");
  timenow = time(NULL);

  if (fptr) {
    fgets(buffer, 256, fptr);
    lastnews = atol(buffer);
    ptr = localtime(&lastnews);
    memcpy(&last_tm, ptr, sizeof(struct tm));
    ptr = localtime(&timenow);
    memcpy(&today_tm, ptr, sizeof(struct tm));

    if (today_tm.tm_yday != last_tm.tm_yday ||
        today_tm.tm_year != last_tm.tm_year) {
      fclose(fptr);
      unlink("dailynews.dat");
      fptr = fopen("dailynews.dat", "w");
      fprintf(fptr, "%ld\n", timenow);
    } else {
      fclose(fptr);
      fptr = fopen("dailynews.dat", "a");
    }
  } else {
    fptr = fopen("dailynews.dat", "w");
    fprintf(fptr, "%ld\n", timenow);
  }

  if (fptr) {
    fprintf(fptr, "%s\n", item);
    fclose(fptr);
  }
}

void send_mail(char *to, char *from, char *msg) {
  char sqlbuffer[1024];
  int rc;
  char *err_msg = 0;
  sqlite3_stmt *stmt;
  sqlite3 *db;
  rc = sqlite3_open("mail.db", &db);
  if (rc) {
    // Error opening the database
    fprintf(stderr, "Error opening mail database: %s\n", sqlite3_errmsg(db));
    sqlite3_close(db);
    md_exit(-2);
  }

  sqlite3_busy_timeout(db, 5000);

  snprintf(sqlbuffer, 1024,
           "CREATE TABLE IF NOT EXISTS mail (id INTEGER PRIMARY KEY, msgto "
           "TEXT COLLATE NOCASE, msgfrom TEXT COLLATE NOCASE, msg TEXT)");
  rc = sqlite3_exec(db, sqlbuffer, 0, 0, &err_msg);
  if (rc != SQLITE_OK) {

    fprintf(stderr, "SQL error: %s", err_msg);

    sqlite3_free(err_msg);
    sqlite3_close(db);

    return;
  }

  snprintf(sqlbuffer, 1024,
           "INSERT INTO mail (msgto, msgfrom, msg) VALUES(?, ?, ?)");
  sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
  sqlite3_bind_text(stmt, 1, to, strlen(to) + 1, SQLITE_STATIC);
  sqlite3_bind_text(stmt, 2, from, strlen(from) + 1, SQLITE_STATIC);
  sqlite3_bind_text(stmt, 3, msg, strlen(msg) + 1, SQLITE_STATIC);

  rc = sqlite3_step(stmt);
  if (rc != SQLITE_DONE) {
    fprintf(stderr, "SQL Error: %s\n", sqlite3_errmsg(db));
  }
  sqlite3_finalize(stmt);
  sqlite3_close(db);
}

void read_mail() {
  char sqlbuffer[1024];
  int rc;
  sqlite3_stmt *stmt;
  sqlite3 *db;
  int gotmail = 0;
  rc = sqlite3_open("mail.db", &db);
  if (rc) {
    // Error opening the database
    fprintf(stderr, "Error opening mail database: %s\n", sqlite3_errmsg(db));
    sqlite3_close(db);
    md_exit(-2);
  }
  sqlite3_busy_timeout(db, 5000);

  snprintf(sqlbuffer, 1024,
           "SELECT msgto, msgfrom, msg FROM mail WHERE msgto=?");
  sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
  sqlite3_bind_text(stmt, 1, info.name, strlen(info.name) + 1, SQLITE_STATIC);

  while (sqlite3_step(stmt) == SQLITE_ROW) {
    md_printf("\r\nMessage from %s\r\n", sqlite3_column_text(stmt, 1));
    md_printf("----------------------------------------------------------------"
              "--\r\n");
    md_printf("%s\r\n", sqlite3_column_text(stmt, 2));
    md_printf("\r\nPress any key to continue....\r\n");
    md_getc();
    gotmail = 1;
  }
  sqlite3_finalize(stmt);

  if (!gotmail) {
    md_printf("\r\nYou have no mail..\r\n");
  } else {
    snprintf(sqlbuffer, 1024, "DELETE FROM mail WHERE msgto=?");
    sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
    sqlite3_bind_text(stmt, 1, info.name, strlen(info.name) + 1, SQLITE_STATIC);
    sqlite3_step(stmt);
    sqlite3_finalize(stmt);
  }
  sqlite3_close(db);
}

void get_random_monster(struct monster_info *monster) {
  int rc;
  sqlite3_stmt *stmt;
  sqlite3 *db;
  char sqlbuffer[256];
  int count;
  int whichMonster;

  rc = sqlite3_open("monsters.db", &db);
  if (rc) {
    // Error opening the database
    fprintf(stderr, "Error opening monsters database: %s\n",
            sqlite3_errmsg(db));
    sqlite3_close(db);
    md_exit(-2);
  }
  sqlite3_busy_timeout(db, 5000);

  snprintf(sqlbuffer, 256, "SELECT COUNT(*) FROM monsterdef WHERE level=?");
  sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
  sqlite3_bind_int(stmt, 1, info.level);
  rc = sqlite3_step(stmt);
  if (rc != SQLITE_ROW) {
    md_printf("Error reading monster database: %s\r\n", sqlite3_errmsg(db));
    sqlite3_finalize(stmt);
    sqlite3_close(db);
    md_exit(-2);
  }

  count = sqlite3_column_int(stmt, 0);

  whichMonster = rand() % count;

  sqlite3_finalize(stmt);

  snprintf(sqlbuffer, 256,
           "SELECT mname,level,toughness,attack1,attack2 FROM monsterdef WHERE "
           "level=? LIMIT ?, 1");
  sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
  sqlite3_bind_int(stmt, 1, info.level);
  sqlite3_bind_int(stmt, 2, whichMonster);

  rc = sqlite3_step(stmt);

  if (rc != SQLITE_ROW) {
    md_printf("Error reading monster database: %s\r\n", sqlite3_errmsg(db));
    sqlite3_finalize(stmt);
    sqlite3_close(db);
    md_exit(-2);
  }

  strncpy(monster->name, sqlite3_column_text(stmt, 0), 32);
  monster->level = sqlite3_column_int(stmt, 1);
  monster->toughness = sqlite3_column_int(stmt, 2);
  strncpy(monster->attack1, sqlite3_column_text(stmt, 3), 64);
  strncpy(monster->attack2, sqlite3_column_text(stmt, 4), 64);
  monster->health = (monster->level * (15 + (monster->toughness * 50))) + (rand() % (monster->level * 25));

  sqlite3_finalize(stmt);
  sqlite3_close(db);
}

int max_health() { return 10 * info.constitution * info.level; }

int get_unassigned_stat_points() {
  int total = (20 + ((info.level - 1) * 2));
  total -= info.constitution;
  total -= info.strength;
  total -= info.dexterity;
  total -= info.wisdom;

  return total;
}

void save_player() {
#if defined(_MSC_VER) || defined(WIN32)
  int fno = open("players.dat", O_WRONLY | O_CREAT | O_BINARY, 0644);
#else
  int fno = open("players.dat", O_WRONLY | O_CREAT, 0644);
#endif
  if (fno == -1) {
    md_exit(-1);
  }
  lseek(fno, sizeof(struct user_info) * player_idx, SEEK_SET);

  write(fno, &info, sizeof(struct user_info));

  close(fno);
}

void add_player_idx() {
  FILE *fptr;

  fptr = fopen("players.idx", "a");
  if (!fptr) {
    fprintf(stderr, "ERROR OPENING players.idx!\n");
    md_exit(0);
  }
  fprintf(fptr, "%s %s+%s\n", mdcontrol.user_firstname, mdcontrol.user_lastname, mdcontrol.user_alias);
  fclose(fptr);
}

int get_player_idx() {
  FILE *fptr;

  char buffer[256];
  char savefile[256];
  int idx = 0;
  fptr = fopen("players.idx", "r");

  snprintf(savefile, 255, "%s %s+%s", mdcontrol.user_firstname, mdcontrol.user_lastname, mdcontrol.user_alias);

  if (fptr != NULL) {

    fgets(buffer, 256, fptr);

    while (!feof(fptr)) {
      if (strncmp(buffer, savefile, strlen(savefile)) == 0) {
        fclose(fptr);
        return idx;
      }
      idx++;
      fgets(buffer, 256, fptr);
    }

    fclose(fptr);
  }


  return -1;
}

int load_player() {

  FILE *fptr;

  player_idx = get_player_idx();

  if (player_idx == -1) {
    return 0;
  }

  fptr = fopen("players.dat", "rb");
  if (!fptr) {
    fprintf(stderr, "players.dat missing! please reset.\n");
    md_exit(0);
  }

  fseek(fptr, sizeof(struct user_info) * player_idx, SEEK_SET);

  if (fread(&info, sizeof(struct user_info), 1, fptr) < 1) {
    fclose(fptr);
    return 0;
  }

  fclose(fptr);

  return 1;
}

int scan_for_player(char *username, struct user_info *userinf) {
  FILE *fptr;

  fptr = fopen("players.dat", "rb");
  if (!fptr) {
    return 0;
  }

  while (fread(userinf, sizeof(struct user_info), 1, fptr) == 1) {
    if (strcasecmp(userinf->name, username) == 0) {
      fclose(fptr);
      return 1;
    }
  }

  fclose(fptr);
  return 0;
}

void save_other_player(struct user_info *userinf) {
  FILE *fptr;
  struct user_info test_info;
  fptr = fopen("players.dat", "r+b");
  if (!fptr) {
    return;
  }

  while (fread(&test_info, sizeof(struct user_info), 1, fptr) == 1) {
    if (strcmp(userinf->name, test_info.name) == 0) {
      fseek(fptr, -sizeof(struct user_info), SEEK_CUR);
      fwrite(userinf, sizeof(struct user_info), 1, fptr);
      fclose(fptr);
      return;
    }
  }

  fclose(fptr);
  return;
}

int calc_critical(struct user_info *ui) {
  float mod = 0;
  if (info.enchant == 2) {
    mod = ui->weaponvalue / 5;
  }
  if (ui->userClass == 1) {
    return ui->level * 5 * ui->strength + ui->weaponvalue + mod;
  } else if (ui->userClass == 2) {
    return ui->level * 5 * ui->strength + ui->weaponvalue + mod;
  } else if (ui->userClass == 3) {
    return ui->level * 5 * ui->wisdom + ui->weaponvalue + mod;
  }
  return 0;
}

int calc_hit(struct user_info *ui) {
  float mod = 0;
  if (info.enchant == 2) {
    mod = ui->weaponvalue / 5;
  }

  if (ui->userClass == 1) {
    return ui->level * 5 / 2 * ui->strength + ui->weaponvalue + mod;
  } else if (ui->userClass == 2) {
    return ui->level * 5 / 2 * ui->strength + ui->weaponvalue + mod;
  } else if (ui->userClass == 3) {
    return ui->level * 5 / 2 * ui->wisdom + ui->weaponvalue + mod;
  }
  return 0;
}

void guild_hall() {
  int done = 0;
  char ch;
  while (!done) {
    md_printf("\r\n");
    if (info.userClass == 1) {
      md_clr_scr();
      md_sendfile("warriorguild.ans", FALSE);
      if (info.level >= 10 && !info.learnedSkill1) {
        md_printf("`white`(`bright white`1`white`) Learn the secret warrior "
                  "skill: `bright white`Megachop`white`\r\n");
      }
      if (info.level >= 20 && !info.learnedSkill2) {
        md_printf("`white`(`bright white`2`white`) Learn the secret warrior "
                  "skill: `bright white`Fury of Fists`white`\r\n");
      }
      if (info.level >= 30 && !info.learnedSkill3) {
        md_printf("`white`(`bright white`3`white`) Learn the secret warrior "
                  "skill: `bright white`Beserker Bludgeon`white`\r\n");
      }
    } else if (info.userClass == 2) {
      md_clr_scr();
      md_sendfile("rogueguild.ans", FALSE);
      if (info.level >= 10 && !info.learnedSkill1) {
        md_printf("`white`(`bright white`1`white`) Learn the secret rogue "
                  "skill: `bright white`Shifty Blade`white`\r\n");
      }
      if (info.level >= 20 && !info.learnedSkill2) {
        md_printf("`white`(`bright white`2`white`) Learn the secret rogue "
                  "skill: `bright white`Fan of Knives`white`\r\n");
      }
      if (info.level >= 30 && !info.learnedSkill3) {
        md_printf("`white`(`bright white`3`white`) Learn the secret rogue "
                  "skill: `bright white`Cheap Shot`white`\r\n");
      }
    } else if (info.userClass == 3) {
      md_clr_scr();
      md_sendfile("wizardguild.ans", FALSE);
      if (info.level >= 10 && !info.learnedSkill1) {
        md_printf("`white`(`bright white`1`white`) Learn the secret wizard "
                  "skill: `bright white`Arcane Blast`white`\r\n");
      }
      if (info.level >= 20 && !info.learnedSkill2) {
        md_printf("`white`(`bright white`2`white`) Learn the secret wizard "
                  "skill: `bright white`Electric Lightning`white`\r\n");
      }
      if (info.level >= 30 && !info.learnedSkill3) {
        md_printf("`white`(`bright white`3`white`) Learn the secret wizard "
                  "skill: `bright white`Lava Burst`white`\r\n");
      }
    }
    md_printf("`white`(`bright white`R`white`) Return to Town`white`\r\n");
    md_printf("\r\nWhat is your command ? ");
    ch = md_get_answer("123Rr");
    md_printf("\r\n");
    if (tolower(ch) == 'r') {
      done = 1;
    } else if (ch == '1' && info.level >= 10 && !info.learnedSkill1) {
      info.learnedSkill1 = 1;
      save_player();
      md_printf("`bright green`Skill learned!`white`\r\n");
    } else if (ch == '2' && info.level >= 20 && !info.learnedSkill2) {
      info.learnedSkill2 = 1;
      save_player();
      md_printf("`bright green`Skill learned!`white`\r\n");
    } else if (ch == '3' && info.level >= 30 && !info.learnedSkill3) {
      info.learnedSkill3 = 1;
      save_player();
      md_printf("`bright green`Skill learned!`white`\r\n");
    }

    md_printf("\r\nPress any key to continue...");
    md_getc();
    md_printf("\r\n");
  }
}

void do_player_battle(struct user_info *otherplayer) {
  int done = 0;
  int validresponse;
  int otherplayerhealth = otherplayer->health;
  int hitval;
  int hchance;
  int runaway;
  char ch;
  char buffer[256];
  int gold;

  md_clr_scr();
  md_sendfile("battle.ans", FALSE);
  md_printf("\r\n`white`You approach `bright white`%s`white` and..\r\n\r\n",
            otherplayer->name);
  while (!done) {


    md_printf("  (A) Attack\r\n");
    md_printf("  (R) Run Away\r\n");
    md_printf("You have `bright green`%d`white` HP left. What do you want to "
              "do? (`bright white`A`white`,`bright white`R`white`) ",
              info.health);
    ch = md_get_answer("AaRr");

    md_printf("\r\n");

    switch (tolower(ch)) {
    case 'a':
      hchance = rand() % 100 + 1;
      if (hchance < 10) {
        hitval = calc_critical(&info);
        hitval -= get_hitval_armor_reduction(hitval, otherplayer->armorvalue);
        md_printf("You smash `bright white`%s`white` for `bright "
                  "green`%d`white` (critical)\r\n",
                  otherplayer->name, hitval);
        otherplayer->health -= hitval;
      } else if (hchance < 30) {
        md_printf("You swing wildly but miss completely.\r\n");
      } else {
        hitval = calc_hit(&info);
        hitval -= get_hitval_armor_reduction(hitval, otherplayer->armorvalue);
        md_printf(
            "You hit `bright white`%s`white` for `bright green`%d`white`\r\n",
            otherplayer->name, hitval);
        otherplayer->health -= hitval;
      }
      break;

    case 'r':
      runaway = rand() % 100 + 1;
      if (runaway < 40 && info.enchant != 1) {
        md_printf("`bright red`You attempt to flee but `bright white`%s`bright "
                  "red` chases you!`white`\r\n",
                  otherplayer->name);
      } else {
        md_printf("`bright green`You successfully flee `bright white`%s`bright "
                  "green`...`white`\r\n",
                  otherplayer->name);
        otherplayer->health = otherplayerhealth;
        save_other_player(otherplayer);
        snprintf(buffer, 256, "%s attacked you, but ran away.", info.name);
        send_mail(otherplayer->name, "Wallis the Watcher", buffer);
        snprintf(buffer, 256, "%s fled from %s in battle.", info.name,
                 otherplayer->name);
        add_news_item(buffer);
        done = 1;
      }
      break;
    }
    if (otherplayer->health <= 0) {
      gold = otherplayer->gold;
      md_printf("`bright green`You have slain `bright white`%s!`bright green` "
                "You loot `bright yellow`%d `bright green`gold.`white`\r\n",
                otherplayer->name, gold);
      info.gold += gold;
      otherplayer->gold = 0;
      snprintf(buffer, 256, "%s attacked you and defeated you.", info.name);
      send_mail(otherplayer->name, "Wallis the Watcher", buffer);
      snprintf(buffer, 256, "%s defeated %s in battle.", info.name,
               otherplayer->name);
      add_news_item(buffer);
      save_player();
      save_other_player(otherplayer);
      done = 1;
    }
    if (!done) {
      hchance = rand() % 100 + 1;
      if (hchance < 10) {
        hitval = calc_critical(otherplayer);
        md_printf("`bright white`%s `white`smashes you for `bright "
                  "red`%d`white` (critical)\r\n",
                  otherplayer->name, hitval);
        info.health -= hitval;
        save_player();
      } else if (hchance < 30) {
        md_printf(
            "`white`You skillfully dodge `bright white`%s`white`'s blow\r\n",
            otherplayer->name);
      } else {
        hitval = calc_hit(otherplayer);
        md_printf(
            "`bright white`%s`white` hits you for `bright red`%d`white`\r\n",
            otherplayer->name);
        info.health -= hitval;
        save_player();
      }
      if (info.health <= 0) {
        md_clr_scr();
        md_sendfile("dead.ans", FALSE);
        md_printf("`white`Press any key to continue...");
        md_getc();
        otherplayer->health = otherplayerhealth;
        otherplayer->gold += info.gold;
        info.gold = 0;
        save_player();
        save_other_player(otherplayer);
        snprintf(buffer, 256, "%s attacked you, but you defeated them.",
                 info.name);
        send_mail(otherplayer->name, "Wallis the Watcher", buffer);
        snprintf(buffer, 256, "%s was defeated by %s in battle.", info.name,
                 otherplayer->name);
        add_news_item(buffer);
        md_exit(0);
      }
      md_printf("`bright white`%s`white` glares at you (`bright "
                "white`%d`white` HP).\r\n",
                otherplayer->name, otherplayer->health);
    }
  }
}

void enchantment_store() {
  char ch;

  md_clr_scr();
  if (info.weaponvalue == 0) {
    md_printf("`bright red`You have no weapon to enchant!\r\n\r\n");
    md_printf("`white`Press any key to continue...");
    md_getc();
    return;
  }
  md_printf(
      "`white`Enchantments Available for `bright white`%s%s`white`:\r\n\r\n",
      get_weapon_name(), get_weapon_enchant());
  md_printf("     (1) ... of Fleetness. (Flee always succeeds)\r\n");
  md_printf("     (2) ... of Sharpness. (Increased Damage)\r\n");
  md_printf("     (3) ... of Regeneration. (Restore Life)\r\n");
  md_printf("     (4) ... of The Hoard. (Increased Treasure)\r\n\r\n");

  md_printf("     (R) Return to Town\r\n");

  md_printf("Each enchantment costs `bright magenta`5`white` gems. You have "
            "`bright magenta`%d`white` gems.\r\n",
            info.gems);

  ch = md_get_answer("1234Rr");

  switch (tolower(ch)) {
  case 'r':
    return;
  case '1':
    if (info.gems < 5) {
      md_printf("`bright red`You can't afford that!`white`\r\n");
      md_printf("Press any key to continue...");
      md_getc();
      return;
    }
    info.gems -= 5;
    info.enchant = 1;
    save_player();
    md_printf(
        "Your `bright white`%s`white` becomes `bright green`%s%s`white`\r\n",
        get_weapon_name(), get_weapon_name(), get_weapon_enchant());
    md_printf("Press any key to continue...");
    md_getc();
    return;
  case '2':
    if (info.gems < 5) {
      md_printf("`bright red`You can't afford that!`white`\r\n");
      md_printf("Press any key to continue...");
      md_getc();
      return;
    }
    info.gems -= 5;
    info.enchant = 2;
    save_player();
    md_printf(
        "Your `bright white`%s`white` becomes `bright green`%s%s`white`\r\n",
        get_weapon_name(), get_weapon_name(), get_weapon_enchant());
    md_printf("Press any key to continue...");
    md_getc();
    return;
  case '3':
    if (info.gems < 5) {
      md_printf("`bright red`You can't afford that!`white`\r\n");
      md_printf("Press any key to continue...");
      md_getc();
      return;
    }
    info.gems -= 5;
    info.enchant = 3;
    save_player();
    md_printf(
        "Your `bright white`%s`white` becomes `bright green`%s%s`white`\r\n",
        get_weapon_name(), get_weapon_name(), get_weapon_enchant());
    md_printf("Press any key to continue...");
    md_getc();
    return;
  case '4':
    if (info.gems < 5) {
      md_printf("`bright red`You can't afford that!`white`\r\n");
      md_printf("Press any key to continue...");
      md_getc();
      return;
    }
    info.gems -= 5;
    info.enchant = 4;
    save_player();
    md_printf(
        "Your `bright white`%s`white` becomes `bright green`%s%s`white`\r\n",
        get_weapon_name(), get_weapon_name(), get_weapon_enchant());
    md_printf("Press any key to continue...");
    md_getc();
    return;
  }
}

void make_announcement() {
  int done = 0;
  char ch;

  char buffer[61];
  char newnewsitem[256];

  while (!done) {
    md_clr_scr();
    md_sendfile("announce.ans", FALSE);
    md_printf(
        "`white`\r\nMaking an announcement costs `bright yellow`500`white` "
        "gold.\r\nYou have `bright yellow`%d`white` gold - are you sure you "
        "want to continue? (`bright white`Y`white`/`bright white`N`white`) ",
        info.gold);
    ch = md_get_answer("YyNn");
    if (tolower(ch) == 'y') {
      if (info.gold < 500) {
        md_printf("`bright red`You can't afford that!`white`\r\n");
        done = 1;
      } else {
        md_printf("Enter your (short) announcement: ");
        md_getstring(buffer, 60, 32, 126);
        if (strlen(buffer) > 0) {
          info.gold -= 500;
          snprintf(newnewsitem, 256, "%s announces: %s", info.name, buffer);
          add_news_item(newnewsitem);
          save_player();
          md_printf("\r\n`bright green`Done.`white`\r\n");
          done = 1;
        } else {
          md_printf("\r\n`bright red`Announcement aborted.`white`\r\n");
          done = 1;
        }
      }
      md_printf("Press any key to continue...\r\n");
      md_getc();
      md_printf("\r\n");
    } else {
      done = 1;
    }
  }
}

void daily_news() {
  FILE *fptr;
  time_t lastnews;
  time_t timenow;
  struct tm today_tm;
  struct tm last_tm;
  struct tm *ptr;

  char buffer[256];
  md_clr_scr();
  md_sendfile("dailynews.ans", FALSE);
  md_printf("\r\n`white`");
  fptr = fopen("dailynews.dat", "r");
  if (!fptr) {
    md_printf("Nothing has made the news today.\r\n\r\n");
  } else {
    fgets(buffer, 256, fptr);
    lastnews = atol(buffer);
    ptr = localtime(&lastnews);
    memcpy(&last_tm, ptr, sizeof(struct tm));
    timenow = time(NULL);
    ptr = localtime(&timenow);
    memcpy(&today_tm, ptr, sizeof(struct tm));

    if (today_tm.tm_yday != last_tm.tm_yday ||
        today_tm.tm_year != last_tm.tm_year) {
      md_printf("Nothing has made the news today.\r\n\r\n");
    } else {
      fgets(buffer, 256, fptr);
      while (!feof(fptr)) {
        buffer[strlen(buffer) - 1] = '\0';
        md_printf("\r\n%s\r\n", buffer);
        fgets(buffer, 256, fptr);
      }
    }

    fclose(fptr);
  }
  md_printf("Press any key to continue...");
  md_getc();
  md_printf("\r\n");
}

void write_mail() {
  char username[32];
  int done = 0;
  struct user_info otheruser;
  char msg[60];
  while (!done) {
    md_clr_scr();
    md_sendfile("mail.ans", FALSE);
    md_printf("\r\n`white`Who do you want to mail: ");
    md_getstring(username, 32, 32, 126);
    if (strlen(username) > 0) {
      if (!scan_for_player(username, &otheruser)) {
        md_printf("\r\n`bright red`I don't know who that is.`white`\r\n");
      } else {
        md_printf("\r\nPlease enter a short message: \r\n");
        md_getstring(msg, 60, 32, 126);
        if (strlen(msg) > 0) {
          send_mail(username, info.name, msg);
          md_printf("\r\n`bright green`Done!`white`\r\n");
          done = 1;
        } else {
          md_printf("\r\n`bright red`Aborted!`white`\r\n");
          done = 1;
        }
      }
    } else {
      md_printf("\r\n`bright red`Aborted!`white`\r\n");
      done = 1;
    }
    md_printf("Press any key to continue...");
    md_getc();
    md_printf("\r\n");
  }
}


int sort_by_XP(const void * rec1, const void * rec2) {
  struct user_info const * ui1 = (struct user_info *)rec1;
  struct user_info const * ui2 = (struct user_info *)rec2;  
  if (ui2->experience > ui1->experience) {
    return 1;
  } else if (ui2->experience < ui1->experience) {
    return -1;
  } else {
    // equal so alpha sort A->Z
    return strcmp(ui1->name, ui2->name);
  }
}

int get_all_players(struct user_info **players, int sortby) {
  // returns array of user_info
  // sortby = 0 (unsorted), 1 (sort by XP)
  // returns total players (caller must free players), or 0 / failure
  FILE * fptr;
  fptr = fopen("players.dat", "rb");
  if (!fptr) {
    *players = NULL;
    return 0;
  }
  
  // get filesize
  fseek(fptr, 0, SEEK_END);
  // calculate total users
  int total = (int)(ftell(fptr) / sizeof(struct user_info));
  fseek(fptr, 0, SEEK_SET);
  *players = (struct user_info *)malloc(total * sizeof(struct user_info));
  int pos = 0;
  if ( fread(*players, sizeof(struct user_info), total, fptr) != total) {
    // failure
    fclose(fptr);
    free(*players);
    *players = NULL;
    return 0;
  }
  fclose(fptr);

  if ( sortby == 1 ) {
    // sort records
    qsort(*players, total, sizeof(struct user_info), sort_by_XP);
  }

  return total;
}

void list_other_players() {
  struct user_info *inf = NULL;
  int total = get_all_players(&inf, 1);
  int pos;

  char *classes[] =  {NULL, "`bright magenta`W`white`", "`bright blue`T`white`", "`bright yellow`Z`white`"};

  md_clr_scr();
  md_sendfile("players.ans", FALSE);

  if (total) {
    for( pos = 0; pos < total; pos++ ) {
      md_printf(
	"                    %s `white`%-32s `bright cyan`%2d       %s\r\n",
        classes[inf[pos].userClass], inf[pos].name, inf[pos].level,
        (inf[pos].health <= 0 ? "`bright red`DEAD`white`"
                         : "`bright green`ALIVE`white`"));
    }
    free(inf);
    inf = NULL;
  }
  
  md_printf("\r\nPress any key to continue...");
  md_getc();
  if (interBBSMode) {
    md_clr_scr();
    md_sendfile("ibbs_scores.ans", FALSE);
    md_printf("\r\n`white`Press any key to continue...");
    md_getc();
  }
}

void build_bbs_scorefile() {
  struct user_info *inf = NULL;
  int total = get_all_players(&inf, 1);
  int pos;
  FILE *fptr, *fptr2;

  // opening in binary mode should disable \n expansion.
  // WIN32 and Linux output should be the same.
  fptr = fopen("bbs_scores.ans", "wb");
  if (fptr) {
    fptr2 = fopen("bbs_score_header.ans", "r");
    if (fptr2) {
      copy_expand(fptr2, fptr);
    }
  }
  
  if (total) {
    for( pos = 0; pos < total; pos++ ) {
      fprintf(fptr, "\x1b[0m %3d. %-31.31s %6u (%d) %s\r\n",
	      pos + 1, inf[pos].name, inf[pos].experience,
	      inf[pos].level, (inf[pos].health <= 0 ? "\x1b[1;31mDEAD\x1b[0m" : "\x1b[1;32mALIVE\x1b[0m"));
    }
  }

  fptr2 = fopen("bbs_score_footer.ans", "r");
  if (fptr2) {
    copy_expand(fptr2, fptr);
  }
  fclose(fptr);

  fptr = fopen("bbs_scores.asc", "wb");
  if (fptr) {
    fptr2 = fopen("bbs_score_header.asc", "r");
    if (fptr2) {
      copy_expand(fptr2, fptr);
    }
  }
  
  if (total) {
    for( pos = 0; pos < total; pos++ ) {
      fprintf(fptr, " %3d. %-31.31s %6u (%d) %s\r\n",
	      pos + 1, inf[pos].name, inf[pos].experience,
	      inf[pos].level, (inf[pos].health <= 0 ? "DEAD" : "ALIVE"));
    }
    free(inf);
  }

  fptr2 = fopen("bbs_score_footer.asc", "r");
  if (fptr2) {
    copy_expand(fptr2, fptr);
  }
  fclose(fptr);

}
 
void bank() {
  int done = 0;
  char ch;
  int amount;
  char buffer[11];
  while (!done) {
    md_clr_scr();
    md_sendfile("thebank.ans", FALSE);
    md_printf("\r\n`white`You currently have `bright yellow`%d`white` gold in "
              "hand and `bright yellow`%d`white` gold in the bank\r\n",
              info.gold, info.goldInBank);
    md_printf("What do you want to do? (`bright white`D`white`,`bright "
              "white`W`white`,`bright white`R`white`) ");
    ch = md_get_answer("DdWwRr");
    md_printf("\r\n\r\n");
    switch (tolower(ch)) {
    case 'd':
      if (info.gold > 0) {
        md_printf("How much gold do you want to deposit? ");
        md_getstring(buffer, 11, '0', '9');
        amount = atoi(buffer);
        if (amount > info.gold) {
          md_printf(
              "\r\n`bright red`You don't have that much on hand!`white`\r\n");
          md_printf("\r\nPress any key to continue...");
          md_getc();
          md_printf("\r\n");
        } else {
          info.gold -= amount;
          info.goldInBank += amount;
          save_player();
        }
      } else {
        md_printf("`bright red`You don't have any gold to deposit`white`\r\n");
        md_printf("\r\nPress any key to continue...");
        md_getc();
        md_printf("\r\n");
      }
      break;
    case 'w':
      if (info.goldInBank > 0) {
        md_printf("How much gold do you want to withdraw? ");
        md_getstring(buffer, 11, '0', '9');
        amount = atoi(buffer);
        if (amount > info.goldInBank) {
          md_printf("\r\n`bright red`You don't have that much in the "
                    "bank!`white`\r\n");
          md_printf("\r\nPress any key to continue...");
          md_getc();
          md_printf("\r\n");
        } else {
          info.goldInBank -= amount;
          info.gold += amount;
          save_player();
        }
      } else {
        md_printf(
            "`bright red`You don't have any gold to withdraw!`white`\r\n");
        md_printf("\r\nPress any key to continue...");
        md_getc();
        md_printf("\r\n");
      }
      break;
    case 'r':
      done = 1;
      break;
    }
  }
}

void training_hall() {
  int done = 0;
  char ch;
  int fight_done;
  int hitval;
  int attack;
  int masters_health = (info.level + 1) * 20;
  int misschance;
  int hchance;
  while (!done) {
    md_clr_scr();
    md_sendfile("training.ans", FALSE);
    if (info.level < 30) {
      md_printf(
          "\r\n   `white`(`bright white`C`white`) Challenge your master.\r\n");
    } else {
      md_printf(
          "\r\n   `white`You have no master, you've beaten them all!\r\n\r\n");
    }
    md_printf("   `white`(`bright white`R`white`) Return to Town\r\n");
    md_printf("\r\nWhat is your command? ");
    ch = md_get_answer("CcRr");
    md_printf("\r\n");
    if (info.level < 30) {
      if (tolower(ch) == 'c') {
        if (info.experience >= pow(info.level, 2) * 1000) {
          if (!info.challengedMasterToday) {
            fight_done = 0;
            md_printf(
                "`white`Your master's skill allows him the first strike!\r\n");
            while (!fight_done) {
              misschance = rand() % 100 + 1;
              if (misschance < info.dexterity - info.level) {
                md_printf(
                    "`white`You skillfully dodge the masters attack.\r\n");
              } else {
                attack = rand() % 100 + 1;
                if (attack < 50) {
                  hitval = rand() % ((info.level + 1) * 5) + 5;
                  hitval = hitval - get_hitval_armor_reduction(hitval, info.armorvalue);
                  md_printf("`white`Your master attacks for `bright "
                            "red`%d`white`\r\n",
                            hitval);
                } else {
                  hitval = rand() % ((info.level + 1) * 7) + 7;
                  hitval = hitval - get_hitval_armor_reduction(hitval, info.armorvalue);
                  md_printf("`white`Your master attacks for `bright "
                            "red`%d`white` (critical)\r\n",
                            hitval);
                }
                info.health -= hitval;
                if (info.health <= 0) {
                  md_printf("`white`You are `bright red`defeated`white`. Your "
                            "master returns you to full health and bids you "
                            "farewell for today.\r\n");
                  info.health = max_health();
                  info.challengedMasterToday = 1;
                  save_player();
                  md_printf("Press any key to continue...");
                  md_getc();
                  md_printf("\r\n");
                  break;
                }
                save_player();
              }

              md_printf("`white`Press any key to attack...");
              md_getc();
              hchance = rand() % 100 + 1;
              if (hchance < 10) {
                hitval = calc_critical(&info);
                md_printf("\r\n`white`You smash your master for `bright "
                          "green`%d`white`. (critical)\r\n",
                          hitval);
                masters_health -= hitval;
              } else if (hchance < 30) {
                md_printf(
                    "\r\n`white`You swing wildly but miss completely.\r\n");
              } else {
                hitval = calc_hit(&info);
                md_printf("\r\n`white`You hit your master for `bright "
                          "green`%d`white`.\r\n",
                          hitval);
                masters_health -= hitval;
              }

              if (masters_health <= 0) {
                md_printf("\r\n`white`Your master bows defeated, you have "
                          "gained a level!\r\n");
                info.level++;
                info.challengedMasterToday = 1;
                info.health = max_health();
                save_player();
                fight_done = 1;
                md_printf("Press any key to continue...");
                md_getc();
                md_printf("\r\n");
              }
              if (!fight_done) {
                md_printf(
                    "\r\n`white`Your master nods and comes to attack...\r\n");
              }
            }
          } else { // Have already challenged master today
            md_printf("`white`Your master is busy attending other students. Come back tomorrow.\r\n");
            md_printf("Press any key to continue...");
            md_getc();
            md_printf("\r\n");
          }
        } else {
          md_printf("`white`Not yet grasshopper, you need at least another "
                    "`bright white`%d`white` experience to beat me.\r\n",
                    (int)pow(info.level, 2) * 1000 - info.experience);
          md_printf("Press any key to continue...");
          md_getc();
          md_printf("\r\n");
        }
      }
    }
    if (tolower(ch) == 'r') {
      done = 1;
    }
  }
}

void inn() {
  int done = 0;
  char ch;
  int tip;
  char buffer[11];
  struct bar_talk bartalk[10];
  struct bar_talk newtalk;
  int r;
  FILE *fptr;
  int i, j;
  while (!done) {
    md_clr_scr();
    md_sendfile("theinn.ans", FALSE);
    md_printf("\r\n`white`Your command ? (`bright white`E`white`,`bright "
              "white`F`white`,`bright white`C`white`,`bright "
              "white`H`white`,`bright white`R`white`) ");
    ch = md_get_answer("EeFfCcHhRr");
    switch (tolower(ch)) {
    case 'e':
      md_clr_scr();
      md_sendfile("roomrent.ans", FALSE);
      md_printf("\r\n`white`Renting a room costs `bright yellow`100`white` "
                "gold.\r\n");
      if (info.gold < 100) {
        md_printf("`bright red`You can't afford that.`white`\r\n");
      } else {
        md_printf("`white`Are you sure you want to rent a room? (`bright "
                  "white`Y`white`/`bright white`N`white`) ");
        ch = md_get_answer("YyNn");
        if (tolower(ch) == 'y') {
          info.gold -= 100;
          md_printf("\r\nDo you want to tip the guards? (You have `bright "
                    "yellow`%d`white` gold) (`bright white`Y`white`/`bright "
                    "white`N`white`)  ? ",
                    info.gold);
          ch = md_get_answer("YyNn");
          md_printf("\r\n");
          if (tolower(ch) == 'y') {
            md_printf("How much: ");
            md_getstring(buffer, 10, '0', '9');
            tip = atoi(buffer);
            if (tip > info.gold) {
              md_printf("\r\n`bright red`You don't have that much, tipping "
                        "nothing.`white`\r\n");
              tip = 0;
            }
            info.gold -= tip;
            info.bribeAmount = tip;
            info.stayingAtInn = 1;
            save_player();
            md_printf("\r\n`bright blue`Nighty night!`white`\r\n");
            md_printf("Press any key to continue...");
            md_getc();
            md_printf("\r\n");
            md_exit(0);
          } else {
            info.bribeAmount = 0;
            info.stayingAtInn = 1;
            save_player();
            md_printf("\r\n`bright blue`Nighty night!`white`\r\n");
            md_printf("Press any key to continue...");
            md_getc();
            md_printf("\r\n");
            md_exit(0);
          }
        }
      }
      md_printf("Press any key to continue...");
      md_getc();
      md_printf("\r\n");
      break;
    case 'f':
      if (!info.flirtedWithAbbey) {
        info.flirtedWithAbbey = 1;
        md_clr_scr();
        md_sendfile("abbey.ans", FALSE);
        md_printf(
            "`white`What do you do? (`bright white`1`white`,`bright "
            "white`2`white`,`bright white`3`white`,`bright "
            "white`4`white`,`bright white`5`white`,`bright red`Q`white`) ");
        ch = md_get_answer("12345Qq");
        r = rand() % 100 + 1;
        switch (ch) {
        case '1':
          if (r < info.charm + 40) {
            md_printf("\r\n`bright magenta`Abbey smiles back! You gain 1 charm "
                      "and 2 luck!`white`\r\n");
            info.charm++;
            info.luck += 2;
          } else {
            md_printf("\r\n`yellow`Abbey scowels at you for wasting her time! "
                      "You lose 1 charm and 2 luck!`white`\r\n");
            info.charm--;
            info.luck -= 2;
          }
          break;
        case '2':
          if (r < info.charm + 25) {
            md_printf("\r\n`bright magenta`Abbey winks back! You gain 2 charm "
                      "and 4 luck!`white`\r\n");
            info.charm += 2;
            info.luck += 4;
          } else {
            md_printf("\r\n`yellow`Abbey scowels at you for wasting her time! "
                      "You lose 2 charm and 4 luck!`white`\r\n");
            info.charm -= 2;
            info.luck -= 4;
          }
          break;
        case '3':
          if (r < info.charm + 15) {
            md_printf("\r\n`bright magenta`Abbey blushes and smiles! You gain "
                      "4 charm and 8 luck!`white`\r\n");
            info.charm += 4;
            info.luck += 8;
          } else {
            md_printf("\r\n`yellow`Abbey rolls her eyes and ignores you! You "
                      "lose 4 charm and 8 luck!`white`\r\n");
            info.charm -= 4;
            info.luck -= 8;
          }
          break;
        case '4':
          if (r < info.charm + 10) {
            md_printf("\r\n`bright magenta`Abbey kisses you deeply! You gain 8 "
                      "charm and 14 luck!`white`\r\n");
            info.charm += 8;
            info.luck += 14;
          } else {
            md_printf("\r\n`yellow`Abbey pushes you away! You lose 8 charm and "
                      "14 luck!`white`\r\n");
            info.charm -= 8;
            info.luck -= 14;
          }
          break;
        case '5':
          if (r < info.charm + 5) {
            md_printf(
                "\r\n`bright magenta`Abbey agrees and takes you upstairs! You "
                "lose your charm and gain 28 luck!`white`\r\n");
            info.charm = 0;
            info.luck += 28;
          } else {
            md_printf("\r\n`yellow`Abbey laughs at the idea and makes fun of "
                      "you! You lose 14 charm and 28 luck!`white`\r\n");
            info.charm -= 14;
            info.luck -= 28;
          }
          break;
        default:
          break;
        }
        if (info.charm < 0) {
          info.charm = 0;
        }
        if (info.luck < 0) {
          info.luck = 0;
        }
        if (info.luck > 50) {
          info.luck = 50;
        }
        save_player();

      } else {
        md_printf("\r\nYou sense Abbey has had enough of you today.\r\n");
      }
      md_printf("Press any key to continue...");
      md_getc();
      md_printf("\r\n");
      break;
    case 'c': {
      md_clr_scr();
      md_sendfile("bartalk.ans", FALSE);
      fptr = fopen("bartalk.dat", "rb");
      if (fptr != NULL) {
        for (i = 0; i < 7; i++) {
          if (fread(&bartalk[i], sizeof(struct bar_talk), 1, fptr) < 1) {
            break;
          }
        }
        fclose(fptr);
        md_printf("\r\n");
      } else {
        md_printf("\r\nThe air is silent, as no one is speaking..\r\n");
        i = 0;
      }
      for (j = 0; j < i; j++) {
        md_printf("`bright white`%s\r\n `white`... says %s\r\n",
                  bartalk[j].line, bartalk[j].name);
      }

      md_printf("\r\n`white`Do you want to add to the conversation? (Y/`bright "
                "green`N`white`) ");
      ch = md_get_answer("YyNn\r");

      if (tolower(ch) == 'y') {
        md_printf("\r\nWhat do you want to say? ");
        md_getstring(newtalk.line, 80, 32, 127);
        if (strlen(newtalk.line) > 0) {
          strcpy(newtalk.name, info.name);

          if (i == 7) {
            j = 1;
          } else {
            j = 0;
          }

          fptr = fopen("bartalk.dat", "wb");
          for (; j < i; j++) {
            fwrite(&bartalk[j], sizeof(struct bar_talk), 1, fptr);
          }
          fwrite(&newtalk, sizeof(struct bar_talk), 1, fptr);
          fclose(fptr);
        }
      }
    } break;
    case 'h':
      if (info.listenedToOldMan == 0) {
        info.listenedToOldMan = 1;
        md_clr_scr();
        md_sendfile("oldman.ans", FALSE);
        md_printf("\r\n");
        r = rand() % 100 + 1;
        if (r < 20) {
          md_printf("The old man tells you a boring story. You lost one fight "
                    "today.\r\n");
          if (info.fights_left > 0) {
            info.fights_left--;
          }
        } else if (r < 40) {
          md_printf("The old man tells you an exciting story. You gain one "
                    "fight today.\r\n");
          info.fights_left++;
        } else if (r < 60) {
          md_printf("The old man tells you a happy story. Your coin purse "
                    "feels heavier.\r\n");
          info.gold += 10;
        } else if (r < 80) {
          md_printf("The old man tells you a sad story. Your coin purse feels "
                    "lighter.\r\n");
          if (info.gold >= 10) {
            info.gold -= 10;
          }
        } else {
          md_printf(
              "The old man tells you an ordinary story. Nothing happens.\r\n");
        }
        save_player();
      } else {
        md_printf("\r\n");
        md_printf("The old man has nothing new to say today.\r\n");
      }
      md_printf("Press any key to continue...");
      md_getc();
      md_printf("\r\n");
      break;
    case 'r':
      done = 1;
      break;
    }
  }
}

void kill_other_players() {
  struct user_info otherplayer;
  char otherplayername[32];
  char ch;
  int bribe;
  char buffer[11];
  md_printf("\r\n");
  if (info.fights_left > 0) {
    md_printf("`white`Please enter the name of the player you wish to kill: ");
    md_getstring(otherplayername, 32, 32, 126);
    if (strlen(otherplayername) == 0) {
	  md_printf("\r\n`bright red`Ok, not today then...`white`\r\n");
    } else if (strcasecmp(otherplayername, info.name) == 0) {
      md_printf("\r\n`bright red`You can't fight yourself...`white`\r\n");
    } else if (!scan_for_player(otherplayername, &otherplayer)) {
      md_printf("\r\n`bright red`I found no such player.`white`\r\n");
    } else if (otherplayer.health <= 0) {
      md_printf(
          "\r\nLooks like someone beat you to it. They're already dead.\r\n");
    } else {
      md_printf("\r\n");
      if (otherplayer.stayingAtInn == 1) {
        md_printf("%s is staying at the inn.\r\n");
        md_printf("Do you want to try and bribe the guards? (Y/N) ");
        ch = md_get_answer("YyNn");
        if (tolower(ch) == 'y') {
          md_printf("\r\nHow much gold do you want to offer? ");
          md_getstring(buffer, 10, '0', '9');
          md_printf("\r\n");
          bribe = atoi(buffer);
          if (bribe > 0) {
            if (bribe > otherplayer.bribeAmount) {
              info.gold -= bribe;
              info.fights_left--;
              save_player();
              do_player_battle(&otherplayer);
            } else {
              md_printf("The guards laugh at your puny offer, and you feel a "
                        "little more tired.\r\n");
              info.fights_left--;
              save_player();
            }
          }
        }
      } else {
        info.fights_left--;
        save_player();
        do_player_battle(&otherplayer);
      }
    }
  } else {
    md_printf("\r\nYou have no fights remaining today...\r\n");
  }
  md_printf("Press any key to continue...");
  md_getc();
  md_printf("\r\n");
}

void view_your_stats() {
  int done = 0;
  char ch;

  while (!done) {
    md_clr_scr();
    md_sendfile("stats.ans", FALSE);
    md_printf("\r\n`bright white` Level `white`%-2d, `bright white`Experience: "
              "`white`%-7d       `bright white`Health: (`white`%-3d `bright "
              "white`of `white`%-3d`bright white`)`white`\r\n",
              info.level, info.experience, info.health, max_health());
    md_printf("`bright white`        Gold in purse: `white`%-5d   `bright "
              "white`Gold in bank: `white`%-5d\r\n",
              info.gold, info.goldInBank);
    md_printf("`bright white`                 Gems: `white`%-5d    `bright "
              "white`Fights Left: `white`%-2d\r\n",
              info.gems, info.fights_left);
    md_printf("`bright white`                Charm: `white`%-3d             "
              "`bright white`Luck: `white`%-3d\r\n",
              info.charm, info.luck);
    md_printf("`bright white`             Strength: `white`%-3d        `bright "
              "white`Dexterity: `white`%-3d\r\n",
              info.strength, info.dexterity);
    md_printf("`bright white`               Wisdom: `white`%-3d     `bright "
              "white`Constitution: `white`%-3d\r\n",
              info.wisdom, info.constitution);
    md_printf("`bright white`                Armor: `white`%s\r\n",
              get_armor_name());
    md_printf("               `bright white`Weapon: `white`%s%s\r\n",
              get_weapon_name(), get_weapon_enchant());

    md_printf(
        "\r\nYou have `bright white`%d`white` unassigned stat points.\r\n",
        get_unassigned_stat_points());
    if (get_unassigned_stat_points() > 0) {
      md_printf("Do you want to increase (`bright white`C`white`)onstitution, "
                "(`bright white`S`white`)trength, (`bright "
                "white`D`white`)exterity\r\n(`bright white`W`white`)isdom or "
                "(`bright white`R`white`)eturn to Town? ");
      ch = md_get_answer("CcSsDdWwRr");
      switch (tolower(ch)) {
      case 'c':
        info.constitution++;
        save_player();
        break;
      case 's':
        info.strength++;
        save_player();
        break;
      case 'd':
        info.dexterity++;
        save_player();
        break;
      case 'w':
        info.wisdom++;
        save_player();
        break;
      case 'r':
        done = 1;
        break;
      }

    } else {
      md_printf("`white`Press any key to continue...");
      md_getc();
      done = 1;
    }
  }
}

void armor_store() {
  int done = 0;
  char ch;
  while (!done) {
    md_clr_scr();
    md_sendfile("armorstore.ans", FALSE);
    md_printf("                   `white`(`bright white`1`white`) Dirty Old "
              "Rags...................`bright yellow`1000`white`");
    if (info.armorvalue == 1) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright white`2`white`) Patchwork "
              "Shirt..................`bright yellow`5000`white`");
    if (info.armorvalue == 5) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright white`3`white`) Leather "
              "Cuiress..................`bright yellow`7000`white`");
    if (info.armorvalue == 7) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright white`4`white`) Rusty Chainmail "
              "Shirt...........`bright yellow`10000`white`");
    if (info.armorvalue == 10) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright white`5`white`) Bronze "
              "Chainmail Shirt..........`bright yellow`15000`white`");
    if (info.armorvalue == 15) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright white`6`white`) Iron Chainmail "
              "Shirt............`bright yellow`20000`white`");
    if (info.armorvalue == 20) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright white`7`white`) Iron "
              "Breastplate................`bright yellow`25000`white`");
    if (info.armorvalue == 25) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright white`8`white`) Steel "
              "Breastplate...............`bright yellow`30000`white`");
    if (info.armorvalue == 30) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright white`9`white`) Mithril "
              "Cuiress.................`bright yellow`35000`white`");
    if (info.armorvalue == 35) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright cyan`R`white`) Return to "
              "Town\r\n\r\n");
    md_printf(
        "You have `bright yellow`%d`white` gold. What do you want to buy? ",
        info.gold);
    ch = md_get_answer("123456789Rr");
    switch (tolower(ch)) {
    case '1':
      if (info.gold < 1000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.armorvalue == 1) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.armorvalue = 1;
        info.gold -= 1000;
        md_printf(
            "\r\nYou are the proud owner of a shiny new dirty old rags!\r\n");
        save_player();
      }
      break;
    case '2':
      if (info.gold < 5000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.armorvalue == 5) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.armorvalue = 5;
        info.gold -= 5000;
        md_printf(
            "\r\nYou are the proud owner of a shiny new patchwork shirt!\r\n");
        save_player();
      }
      break;
    case '3':
      if (info.gold < 7000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.armorvalue == 7) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.armorvalue = 7;
        info.gold -= 7000;
        md_printf(
            "\r\nYou are the proud owner of a shiny new leather cuiress!\r\n");
        save_player();
      }
      break;
    case '4':
      if (info.gold < 10000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.armorvalue == 10) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.armorvalue = 10;
        info.gold -= 10000;
        md_printf("\r\nYou are the proud owner of a shiny new rusty chainmail "
                  "shirt!\r\n");
        save_player();
      }
      break;
    case '5':
      if (info.gold < 15000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.armorvalue == 15) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.armorvalue = 15;
        info.gold -= 15000;
        md_printf("\r\nYou are the proud owner of a shiny new bronze chainmail "
                  "shirt!\r\n");
        save_player();
      }
      break;
    case '6':
      if (info.gold < 20000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.armorvalue == 20) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.armorvalue = 20;
        info.gold -= 20000;
        md_printf("\r\nYou are the proud owner of a shiny new iron chainmail "
                  "shirt!\r\n");
        save_player();
      }
      break;
    case '7':
      if (info.gold < 25000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.armorvalue == 25) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.armorvalue = 25;
        info.gold -= 25000;
        md_printf(
            "\r\nYou are the proud owner of a shiny new iron breastplate!\r\n");
        save_player();
      }
      break;
    case '8':
      if (info.gold < 30000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.armorvalue == 30) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.armorvalue = 30;
        info.gold -= 30000;
        md_printf("\r\nYou are the proud owner of a shiny new steel "
                  "breastplate!\r\n");
        save_player();
      }
      break;
    case '9':
      if (info.gold < 35000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.armorvalue == 35) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.armorvalue = 35;
        info.gold -= 35000;
        md_printf(
            "\r\nYou are the proud owner of a shiny new mithril cuiress!\r\n");
        save_player();
      }
      break;
    case 'r':
      done = 1;
      break;
    }
    if (!done) {
      md_printf("Press any key to continue...");
      md_getc();
    }
  }
}

void weapon_store() {
  int done = 0;
  char ch;
  while (!done) {
    md_clr_scr();
    md_sendfile("weaponstore.ans", FALSE);
    md_printf("                   `white`(`bright white`1`white`) Large "
              "Stick......................`bright yellow`1000`white`");
    if (info.weaponvalue == 1) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright white`2`white`) "
              "Dagger...........................`bright yellow`5000`white`");
    if (info.weaponvalue == 5) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright white`3`white`) Knotty "
              "Club......................`bright yellow`7000`white`");
    if (info.weaponvalue == 7) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright white`4`white`) Rusty Old "
              "Sword.................`bright yellow`10000`white`");
    if (info.weaponvalue == 10) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright white`5`white`) "
              "Rapier..........................`bright yellow`15000`white`");
    if (info.weaponvalue == 15) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright white`6`white`) "
              "Cutlass.........................`bright yellow`20000`white`");
    if (info.weaponvalue == 20) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright white`7`white`) Short "
              "Sword.....................`bright yellow`25000`white`");
    if (info.weaponvalue == 25) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright white`8`white`) Long "
              "Sword......................`bright yellow`30000`white`");
    if (info.weaponvalue == 30) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright white`9`white`) Bastard "
              "Sword...................`bright yellow`35000`white`");
    if (info.weaponvalue == 35) {
      md_printf(" (You have this)");
    }
    md_printf("\r\n                   (`bright cyan`R`white`) Return to "
              "Town\r\n\r\n");
    md_printf(
        "You have `bright yellow`%d`white` gold. What do you want to buy? ",
        info.gold);
    ch = md_get_answer("123456789Rr");
    switch (tolower(ch)) {
    case '1':
      if (info.gold < 1000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.weaponvalue == 1) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.weaponvalue = 1;
        info.gold -= 1000;
        info.enchant = 0;
        md_printf(
            "\r\nYou are the proud owner of a shiny new large stick!\r\n");
        save_player();
      }
      break;
    case '2':
      if (info.gold < 5000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.weaponvalue == 5) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.weaponvalue = 5;
        info.gold -= 5000;
        info.enchant = 0;
        md_printf("\r\nYou are the proud owner of a shiny new dagger!\r\n");
        save_player();
      }
      break;
    case '3':
      if (info.gold < 7000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.weaponvalue == 7) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.weaponvalue = 7;
        info.gold -= 7000;
        info.enchant = 0;
        md_printf(
            "\r\nYou are the proud owner of a shiny new knotty club!\r\n");
        save_player();
      }
      break;
    case '4':
      if (info.gold < 10000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.weaponvalue == 10) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.weaponvalue = 10;
        info.gold -= 10000;
        info.enchant = 0;
        md_printf(
            "\r\nYou are the proud owner of a shiny new rusty old sword!\r\n");
        save_player();
      }
      break;
    case '5':
      if (info.gold < 15000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.weaponvalue == 15) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.weaponvalue = 15;
        info.gold -= 15000;
        info.enchant = 0;
        md_printf("\r\nYou are the proud owner of a shiny new rapier!\r\n");
        save_player();
      }
      break;
    case '6':
      if (info.gold < 20000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.weaponvalue == 20) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.weaponvalue = 20;
        info.gold -= 20000;
        info.enchant = 0;
        md_printf("\r\nYou are the proud owner of a shiny new cutlass!\r\n");
        save_player();
      }
      break;
    case '7':
      if (info.gold < 25000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.weaponvalue == 25) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.weaponvalue = 25;
        info.gold -= 25000;
        info.enchant = 0;
        md_printf(
            "\r\nYou are the proud owner of a shiny new short sword!\r\n");
        save_player();
      }
      break;
    case '8':
      if (info.gold < 30000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.weaponvalue == 30) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.weaponvalue = 30;
        info.gold -= 30000;
        info.enchant = 0;
        md_printf("\r\nYou are the proud owner of a shiny new long sword!\r\n");
        save_player();
      }
      break;
    case '9':
      if (info.gold < 35000) {
        md_printf("\r\nYou can't afford that!\r\n");
      } else if (info.weaponvalue == 35) {
        md_printf("\r\nYou already own one of those!\r\n");
      } else {
        info.weaponvalue = 35;
        info.gold -= 35000;
        info.enchant = 0;
        md_printf(
            "\r\nYou are the proud owner of a shiny new bastard sword!\r\n");
        save_player();
      }
      break;
    case 'r':
      done = 1;
      break;
    }
    if (!done) {
      md_printf("Press any key to continue...");
      md_getc();
    }
  }
}

void healers() {
  int tenpercent;
  int tenpercentcost;
  int fiftypercent;
  int fiftypercentcost;
  int hundredpercent;
  int hundredpercentcost;
  int potioncost = (int)(((float)max_health() / 100.f) * 10.f);
  char ch;

  int done = 0;
  while (!done) {
    md_clr_scr();
    md_sendfile("healers.ans", FALSE);
    md_printf("\r\n`bright white`Your Health: ");
    print_bar(info.health, max_health(), "bright red");
    md_printf("\r\n\r\n`white`You have `bright yellow`%d`white` gold, and `bright white`%d`white` potions.\r\n", info.gold, info.potions);
    if (info.health == max_health()) {
      md_printf("\r\nYou look fit and healthy!\r\n");
      md_printf(" (`bright white`P`white`) Buy a potion for `bright yellow`%d`white` gold.\r\n", potioncost);
      md_printf(" (`bright white`L`white`) Leave\r\n");
      md_printf(
          "\r\nWhat is your command? (`bright white`P`white`,`bright white`L`white`) ");
      ch = md_get_answer("PpLl");
      md_printf("\r\n\r\n");      
      switch(ch) {
      case 'p':
        if (info.gold < potioncost) {
          md_printf("`bright red`You can't afford that!`white`\r\n");
        } else {
          info.gold -= potioncost;
          info.potions++;
          save_player();
          md_printf("`bright green`You bought a potion!`white\r\n");
        }
        break;
      case 'l':
        done = 1;
        break;
      }
    } else {
      tenpercent =
          (int)(((float)max_health() - (float)info.health) / 100.f * 10.f) + 1;
      tenpercentcost = tenpercent / 2 + 1;
      fiftypercent =
          (int)(((float)max_health() - (float)info.health) / 100.f * 50.f) + 1;
      fiftypercentcost = fiftypercent / 2 + 1;
      hundredpercent = max_health() - info.health;
      hundredpercentcost = hundredpercent / 2 + 1;

      md_printf("\r\n (`bright white`1`white`) Heal `bright white`%d`white` HP for "
                "`bright yellow`%d`white` gold.\r\n",
                tenpercent, tenpercentcost);
      md_printf(" (`bright white`2`white`) Heal `bright white`%d`white` HP for "
                "`bright yellow`%d`white` gold.\r\n",
                fiftypercent, fiftypercentcost);
      md_printf(" (`bright white`3`white`) Heal `bright white`%d`white` HP for "
                "`bright yellow`%d`white` gold.\r\n",
                hundredpercent, hundredpercentcost);
      md_printf(" (`bright white`P`white`) Buy a potion for `bright yellow`%d`white` gold.\r\n", potioncost);
      md_printf(" (`bright white`L`white`) Leave\r\n");
      md_printf(
          "\r\nWhat is your command? (`bright white`1`white`,`bright "
          "white`2`white`,`bright white`3`white`,`bright white`P`white`,`bright white`L`white`) ");
      ch = md_get_answer("123PpLl");
      md_printf("\r\n\r\n");
      switch (tolower(ch)) {
      case '1':
        if (info.gold < tenpercentcost) {
          md_printf("`bright red`You can't afford that!`white`\r\n");
        } else {
          md_printf("`bright green`You feel a little better!`white`\r\n");
          info.gold -= tenpercentcost;
          info.health += tenpercent;
          save_player();
        }
        break;
      case '2':
        if (info.gold < fiftypercentcost) {
          md_printf("`bright red`You can't afford that!`white`\r\n");
        } else {
          md_printf("`brigt green`You feel much better!`white`\r\n");
          info.gold -= fiftypercentcost;
          info.health += fiftypercent;
          save_player();
        }
        break;
      case '3':
        if (info.gold < hundredpercentcost) {
          md_printf("`bright red`You can't afford that!`white`\r\n");
        } else {
          md_printf("`bright green`You feel wonderful!`white`\r\n");
          info.gold -= hundredpercentcost;
          info.health += hundredpercent;
          save_player();
        }
        break;
      case 'p':
        if (info.gold < potioncost) {
          md_printf("`bright red`You can't afford that!`white`\r\n");
        } else {
          info.gold -= potioncost;
          info.potions++;
          save_player();
          md_printf("`bright green`You bought a potion!`white\r\n");
        }
        break;
      case 'l':
        done = 1;
        break;
      }
    }
    md_printf("Press any key to continue...");
    md_getc();
    md_printf("\r\n");
  }
}

void look_for_something_to_kill() {
  struct monster_info monster;
  int done = 0;
  int validresponse = 0;
  int hitval;
  char ch;
  int hchance;
  int runaway;
  int gold;
  int xp;
  int misschance;
  int attack;
  char buffer[256];
  int gems;
  int health_mod;
  int monster_max_health;
  int potions;
  int treasure_map;

  get_random_monster(&monster);
  md_printf("`bright black`------------------------------------------------------------------------------`white`\r\n");
  md_printf("\r\nYou come across a mean looking `bright blue`%s`white` "
            "(`bright red`%d HP`white`)!\r\n\r\n",
            monster.name, monster.health);

  monster_max_health = monster.health;
  while (!done) {
    validresponse = 0;
    while (!validresponse) {


      md_printf("`bright white` Your Health: ");
      print_bar(info.health, max_health(), "bright red");
      md_printf("\r\n`bright white`Enemy Health: ");
      print_bar(monster.health, monster_max_health, "bright yellow");
      md_printf("\r\n\r\nDo you want to:\r\n\r\n");
      md_printf("   (`bright white`A`white`) Attack\r\n");
      if (info.potions > 0) {
          md_printf("   (`bright white`Q`white`) Quaff a potion (%d left)\r\n", info.potions);
      }

      if (info.learnedSkill1 && info.skillPoints > 0) {
        switch (info.userClass) {
        case 1:
          md_printf("    (`bright magenta`W`white`) Use your warrior skills "
                    "(%d SP)\r\n",
                    info.skillPoints);
          break;
        case 2:
          md_printf("    (`bright blue`T`white`) Use your thieving skills (%d "
                    "SP)\r\n",
                    info.skillPoints);
          break;
        case 3:
          md_printf("    (`bright cyan`C`white`) Cast a spell (%d SP)\r\n",
                    info.skillPoints);
          break;
        }
      }
      md_printf("   (`bright white`R`white`) Run Away\r\n");

      md_printf("`bright black`------------------------------------------------------------------------------`white`\r\n");
      ch = md_get_answer("AaRrWwTtCcQq");
      md_printf("\r\n\r\n");
      switch (tolower(ch)) {
      case 'q':
        if (info.potions > 0) {
          info.potions--;
          save_player();

          if (info.health >= max_health()) {
            md_printf("You drink a potion and feel indifferent!\r\n");
          } else {
            info.health += (int)(((float)max_health() / 100.f) * 33.f);
            if (info.health > max_health()) {
              info.health = max_health();
            }
            md_printf("You drink a potion and feel a little better!\r\n");
          }
          validresponse = 1;
        }
        break;
      case 'a':
        hchance = rand() % 100 + 1;
        if (hchance < 10) {
          hitval = calc_critical(&info);
          md_printf("You smash `bright blue`%s`white` for `bright "
                    "green`%d`white` (critical)\r\n",
                    monster.name, hitval);
          monster.health -= hitval;
        } else if (hchance < 30) {
          md_printf("You swing wildly but miss completely.\r\n");
        } else {
          hitval = calc_hit(&info);
          md_printf(
              "You hit `bright blue`%s`white` for `bright green`%d`white`.\r\n",
              monster.name, hitval);
          monster.health -= hitval;
        }
        validresponse = 1;
        break;
      case 'r':
        runaway = rand() % 100 + 1;
        if (runaway < 40 && info.enchant != 1) {
          md_printf(
              "You attempt to flee but `bright blue`%s`white` chases you!\r\n",
              monster.name);
        } else {
          md_printf("You successfully flee `bright blue`%s`white`\r\n",
                    monster.name);
          done = 1;
        }
        validresponse = 1;
        break;
      case 'w':
        if (info.userClass == 1 && (info.learnedSkill1 || info.learnedSkill2 || info.learnedSkill3)) {
          if (info.learnedSkill1 && info.skillPoints >= 1) {
            md_printf("1. Mighty megachop (1 SP)\r\n");
          }
          if (info.learnedSkill2 && info.skillPoints >= 2) {
            md_printf("2. Fury of fists (2 SP)\r\n");
          }
          if (info.learnedSkill3 && info.skillPoints >= 3) {
            md_printf("2. Beserker bludgeon (3 SP)\r\n");
          }
          ch = md_get_answer("123");
          if (ch == '1' && info.learnedSkill1 && info.skillPoints >= 1) {
            hitval = calc_critical(&info) + 5 * info.level;
            md_printf("You raise your arms to the sky and bring them crashing "
                      "down in a mighty megachop!\r\n");
            md_printf("`bright blue`%s`white` takes `bright green`%d`white` "
                      "damage!\r\n",
                      monster.name, hitval);
            monster.health -= hitval;
            info.skillPoints -= 1;
          }
          if (ch == '2' && info.learnedSkill2 && info.skillPoints >= 2) {
            hitval = calc_critical(&info) + 10 * info.level;
            md_printf("You burst into a fury of fists!\r\n");
            md_printf("`bright blue`%s`white` takes `bright green`%d`white` "
                      "damage!\r\n",
                      monster.name, hitval);
            monster.health -= hitval;
            info.skillPoints -= 2;
          }
          if (ch == '3' && info.learnedSkill3 && info.skillPoints >= 3) {
            hitval = calc_critical(&info) + 20 * info.level;
            md_printf(
                "Your rage takes over with a mighty beserker bludgeon!\r\n");
            md_printf("`bright blue`%s`white` takes `bright green`%d`white` "
                      "damage!\r\n",
                      monster.name, hitval);
            monster.health -= hitval;
            info.skillPoints -= 3;
          }
          validresponse = 1;
        } else {
          md_printf("Huh?\r\n");
        }
        break;
      case 't':
        if (info.userClass == 2 && (info.learnedSkill1 || info.learnedSkill2 || info.learnedSkill3)) {
          if (info.learnedSkill1 && info.skillPoints >= 1) {
            md_printf("1. Shifty blade (1 SP)\r\n");
          }
          if (info.learnedSkill2 && info.skillPoints >= 2) {
            md_printf("2. Fan of knives (2 SP)\r\n");
          }
          if (info.learnedSkill3 && info.skillPoints >= 3) {
            md_printf("2. Cheap shot (3 SP)\r\n");
          }
          ch = md_get_answer("123");
          if (ch == '1' && info.learnedSkill1 && info.skillPoints >= 1) {
            hitval = calc_critical(&info) + 5 * info.level;
            md_printf("You slide a hidden blade into %s's ribs!\r\n",
                      monster.name);
            md_printf("`bright blue`%s`white` takes `bright green`%d`white` "
                      "damage!\r\n",
                      monster.name, hitval);
            monster.health -= hitval;
            info.skillPoints -= 1;
          }
          if (ch == '2' && info.learnedSkill2 && info.skillPoints >= 2) {
            hitval = calc_critical(&info) + 10 * info.level;
            md_printf("You toss all of your knives before you!\r\n");
            md_printf("`bright blue`%s`white` takes `bright green`%d`white` "
                      "damage!\r\n",
                      monster.name, hitval);
            monster.health -= hitval;
            info.skillPoints -= 2;
          }
          if (ch == '3' && info.learnedSkill3 && info.skillPoints >= 3) {
            hitval = calc_critical(&info) + 20 * info.level;
            md_printf("You swallow your pride and take a cheap shot!\r\n");
            md_printf("`bright blue`%s`white` takes `bright green`%d`white` "
                      "damage!\r\n",
                      monster.name, hitval);
            monster.health -= hitval;
            info.skillPoints -= 3;
          }
          validresponse = 1;
        } else {
          md_printf("Huh?\r\n");
        }
        break;
      case 'c':
        if (info.userClass == 3 && (info.learnedSkill1 || info.learnedSkill2 || info.learnedSkill3)) {
          if (info.learnedSkill1 && info.skillPoints >= 1) {
            md_printf("1. Arcane blast (1 SP)\r\n");
          }
          if (info.learnedSkill2 && info.skillPoints >= 2) {
            md_printf("2. Electric lightning (2 SP)\r\n");
          }
          if (info.learnedSkill3 && info.skillPoints >= 3) {
            md_printf("2. Lava burst (3 SP)\r\n");
          }
          ch = md_get_answer("123");
          if (ch == '1' && info.learnedSkill1 && info.skillPoints >= 1) {
            hitval = calc_critical(&info) + 5 * info.level;
            md_printf(
                "You mumble some wizardly words and cast an arcane blast!\r\n",
                monster.name);
            md_printf("`bright blue`%s`white` takes `bright green`%d`white` "
                      "damage!\r\n",
                      monster.name, hitval);
            monster.health -= hitval;
            info.skillPoints -= 1;
          }
          if (ch == '2' && info.learnedSkill2 && info.skillPoints >= 2) {
            hitval = calc_critical(&info) + 10 * info.level;
            md_printf("You call out to the heavens and deliver electric "
                      "lightning!\r\n");
            md_printf("`bright blue`%s`white` takes `bright green`%d`white` "
                      "damage!\r\n",
                      monster.name, hitval);
            monster.health -= hitval;
            info.skillPoints -= 2;
          }
          if (ch == '3' && info.learnedSkill3 && info.skillPoints >= 3) {
            hitval = calc_critical(&info) + 20 * info.level;
            md_printf("Your eyes turn to flame as you cast a mighty ball of "
                      "lava!\r\n");
            md_printf("`bright blue`%s`white` takes `bright green`%d`white` "
                      "damage!\r\n",
                      monster.name, hitval);
            monster.health -= hitval;
            info.skillPoints -= 3;
          }
          validresponse = 1;
        } else {
          md_printf("Huh?\r\n");
        }
        break;
      }
    }
    if (monster.health <= 0) {
      gems = 0;
      if (rand() % 100 + 1 > 95) {
        gems = 1;
      }
      potions = 0;
      if (rand() % 100 + 1 > 75) {
        potions = 1;
      }
      if (rand() % 100 + 1 > 95 && info.treasuremap == 0) {
        treasure_map = 1;
      }
      if (info.enchant == 4) {
        gold = rand() % (info.level * 11) + 25;
      } else {
        gold = rand() % (info.level * 10) + 25;
      }
      xp = monster.level * 10 + (monster.toughness * 5);
      md_printf(
          "You have slain `bright blue`%s`white`! You loot`bright yellow` "
          "%d`white` gold, and gain `bright white`%d`white` experience.\r\n",
          monster.name, gold, xp);
      if (gems == 1) {
        md_printf("\r\n`bright white`You found a `bright magenta`Gem`bright "
                  "white`!\r\n");
        info.gems += gems;
      }
      if (potions == 1) {
        md_printf("\r\n`bright white`You found a `bright red`Potion`bright white`!\r\n");
        info.potions += potions;
      }
      if (treasure_map == 1) {
        md_printf("\r\n`bright white`You found a `brigh cyan`Treasure Map`bright white`!\r\n");
        info.treasuremap = 1;
      }
      info.gold += gold;
      info.experience += xp;
      save_player();
      done = 1;
    }

    if (done != 1) {
      misschance = rand() % 100 + 1;
      if (misschance < info.dexterity - info.level) {
        md_printf("You skillfully dodge `bright blue`%s's`white` attack.\r\n",
                  monster.name);
      } else {
        attack = rand() % 100 + 1;
        if (attack > 50) {
          hitval = rand() % (monster.level * 5) + 5;
          hitval = hitval - get_hitval_armor_reduction(hitval, info.armorvalue);
          md_printf("`bright blue`%s`white` %s for `bright red`%d`white`\r\n",
                    monster.name, monster.attack1, hitval);
        } else {
          hitval = rand() % (monster.level * 7) + 7;
          hitval = hitval - get_hitval_armor_reduction(hitval, info.armorvalue);
          md_printf("`bright blue`%s`white` %s for `bright red`%d`white`\r\n",
                    monster.name, monster.attack2, hitval);
        }
        info.health -= hitval;
        save_player();
        if (info.health <= 0) {
          md_clr_scr();
          md_sendfile("dead.ans", FALSE);
          md_printf("Press any key to continue...");
          md_getc();
          snprintf(buffer, 256, "%s was killed by a %s.", info.name,
                   monster.name);
          add_news_item(buffer);
          info.gold = 0;
          save_player();
          md_exit(0);
        }
      }
      if (info.enchant == 3 && info.health < max_health()) {
        health_mod = (int)((float)max_health() * 0.05f);
        info.health += health_mod;
        if (info.health > max_health()) {
          info.health = max_health();
        }
        md_printf("`bright green`You regenerate %d health!`white`\r\n",
                  health_mod);
      }
      md_printf("\r\n`bright black`------------------------------------------------------------------------------\r\n");
      md_printf("`bright blue`%s`white` glares at you....\r\n\r\n",
                monster.name);
    }
  }
}

void enter_the_woods() {
  int done = 0;
  char ch;
  int eventrand;
  int whichevent;
  int doneevent = 0;
  int lucky;
  char buffer[256];
  int goldfound;

  while (!done) {
    md_clr_scr();
    md_sendfile("thewoods.ans", FALSE);
    

    md_printf("\r\n`bright white`    Your Health: ");
    print_bar(info.health, max_health(), "bright red");

    md_printf("\r\n`bright white`Your Experience: ");
    print_bar(info.experience, (int)pow(info.level, 2) * 1000, "bright green");


    md_printf("\r\n\r\n`white`You have `bright white`%d`white` fights left today and `bright white`%d`white` potions on hand.\r\n",
              info.fights_left, info.potions);
    md_printf("\r\n`white`(`bright white`L`white`) Look for something to kill  "
              "    (`bright white`H`white`) Healer\r\n");
    if (info.treasuremap) {
      md_printf("`white`(`bright white`X`white`) Hunt for treasure!\r\n");
    }
    md_printf("(`bright white`R`white`) Return to town\r\n");

    ch = md_get_answer("LlHhXxRr");

    switch (tolower(ch)) {
    case 'x':
      if (info.treasuremap == 1) {
        eventrand = rand() % 100 + 1;
        if (eventrand < 25) {
          md_printf("\r\n\r\nNothing interesting found...\r\n");
        } else if (eventrand < 75) {
          goldfound = (info.level * 50) + (rand() % 1000);
          md_printf("\r\n\r\nYou found `bright yellow`%d`white` gold!\r\n", goldfound);
          info.gold += goldfound;
        } else {
          look_for_something_to_kill();
        }
        md_printf("\r\nYour treasure map crumbles and is no more.\r\n");

        info.treasuremap = 0;
        save_player();
        md_printf("\r\n`white`Press any key to continue...");
        md_getc();        
      }
      break;
    case 'l':
      if (info.fights_left > 0) {
        info.fights_left--;
        save_player();

        doneevent = 0;

        eventrand = rand() % 100 + 1;
        if (eventrand < 10) {
          whichevent = rand() % 100 + 1;

          if (whichevent < 33 && info.charm < 5) {
            md_clr_scr();
            md_sendfile("ugly.ans", FALSE);
            info.charm += 10;
            save_player();
            doneevent = 1;
          } else if (whichevent < 66) {
            md_clr_scr();
            md_sendfile("fountain.ans", FALSE);
            ch = md_get_answer("YyNn");
            if (tolower(ch) == 'y') {
              lucky = rand() % 100 + info.luck;
              if (lucky > 75) {
                md_printf("`white`The fountain invigorates you! You gain "
                          "`bright white`5`white` more fights today!\r\n");
                info.fights_left += 5;
                save_player();
              } else {
                md_printf("`white`The fountain makes you drowsy. You lose "
                          "`bright white`5`white` fights today...\r\n");
                info.fights_left -= 5;
                if (info.fights_left < 0) {
                  info.fights_left = 0;
                }
                save_player();
              }
              doneevent = 1;
            }
          } else if (whichevent > 66) {
            md_clr_scr();
            md_sendfile("cave.ans", FALSE);
            ch = md_get_answer("YyNn");
            if (tolower(ch) == 'y') {
              lucky = rand() % 100 + info.luck;
              if (lucky > 75) {
                md_clr_scr();
                md_sendfile("hoard.ans", FALSE);

                info.gold += 100 * info.level;
                info.luck -= 25;
                if (info.luck < 0) {
                  info.luck = 0;
                }
                save_player();
                md_printf("\r\n`white`You loot `bright yellow`%d`white gold.",
                          100 * info.level);
              } else {
                md_clr_scr();
                md_sendfile("dragon.ans", FALSE);

                info.gold = 0;
                info.health = 0;

                save_player();
                snprintf(buffer, 256, "Curiosity killed %s.", info.name);
                add_news_item(buffer);
                md_printf("\r\n`white`You are dead, please come back "
                          "tomorrow.. Press any key to exit.");
                md_getc();
                md_exit(0);
              }
            }
            doneevent = 1;
          }
        }
        if (!doneevent) {
          look_for_something_to_kill();
        }
        md_printf("\r\n`white`Press any key to continue...");
        md_getc();
      } else {
        md_printf("`bright yellow`You're too tired to fight anymore "
                  "today.`white`\r\n");
        md_printf("Press any key to continue...");
        md_getc();
      }
      break;
    case 'h':
      healers();
      break;
    case 'r':
      done = 1;
      break;
    }
  }
}

void play_game() {
  int name_taken = 1;
  struct user_info otherplayer;
  int userClass = 0;
  int constitution = 0;
  int strength = 0;
  int dexterity = 0;
  int wisdom = 0;
  int done = 0;
  int stats = 20;
  char ch;
  time_t timenow;
  struct tm today_tm;
  struct tm last_tm;
  struct tm *ptr;
  char newsitem[256];
  
  if (!load_player()) {
    md_clr_scr();

    md_sendfile("welcome.ans", TRUE);

    md_printf("\r\nPress any key to begin...");
    md_getc();
    while (name_taken) {
      md_printf("\r\n\r\n`white`You look new around here, what's your name? ");
      md_getstring(info.name, 32, 32, 126);
      if (strlen(info.name) == 0) {
        md_printf("\r\n\r\nOk, see you later then.\r\n");
        md_printf("Press any key to continue...");
        md_getc();
        md_printf("\r\n");
        return;
      }
      if (strlen(info.name) < 3) {
        md_printf("\r\n\r\n`bright red`Surely your name is longer than that! "
                  "Let's try again shall we?`white`\r\n\r\n");
      } else {
        if (scan_for_player(info.name, &otherplayer)) {
          md_printf("\r\n\r\n`bright red`Sorry, that name is taken, Let's try "
                    "again shall we?`white`\r\n\r\n");
        } else {
          name_taken = 0;
        }
      }
    }

    md_printf("\r\n\r\nWelcome `bright white`%s`white`, tell me a bit about "
              "yourself...\r\n",
              info.name);

    while (!done) {
      md_printf("\r\nAs a child, you remember mostly: \r\n");
      md_printf("     `white`(`bright white`1`white`) `bright blue`Frying ants "
                "with a magnifying glass`white`\r\n");
      md_printf("     `white`(`bright white`2`white`) `bright magenta`Lying, "
                "stealing and cheating`white`\r\n");
      md_printf(
          "     `white`(`bright white`3`white`) `bright yellow`Concentrating "
          "really hard to make your sister's head explode.`white`\r\n\r\n");
      md_printf(":");
      userClass = md_get_answer("123") - '0';

      md_printf("\r\n\r\n");
      md_printf("Sounds like your a ");
      stats = 6;
      switch (userClass) {
      case 1:
        md_printf("`bright blue`Warrior`white` with the following stats:\r\n");
        
        strength = 6;
        constitution = 6;
        wisdom = 1;
        dexterity = 1;
        
		while (stats > 0) {
			switch(rand() % 4 + 1) {
				case 1:
					wisdom += 1;
					break;
				case 2:
					dexterity += 1;
					break;
				case 3:
					strength += 1;
					break;
				default:
					constitution += 1;
					break;
			}
			stats--;
		}

        md_printf("Constitution: `bright white`%d`white`\r\n", constitution);
        md_printf("    Strength: `bright white`%d`white`\r\n", strength);
        md_printf("   Dexterity: `bright white`%d`white`\r\n", dexterity);
        md_printf("      Wisdom: `bright white`%d`white`\r\n", wisdom);

        md_printf("\r\n");

        break;
      case 2:
        md_printf("`bright magenta`Rogue`white` with the following stats:\r\n");
        dexterity = 6;
        constitution = 4;
        wisdom = 1;
        strength = 3;
        
		while (stats > 0) {
			switch(rand() % 4 + 1) {
				case 1:
					wisdom += 1;
					break;
				case 2:
					dexterity += 1;
					break;
				case 3:
					strength += 1;
					break;
				default:
					constitution += 1;
					break;
			}
			stats--;
		}

        md_printf("Constitution: `bright white`%d`white`\r\n", constitution);
        md_printf("   Dexterity: `bright white`%d`white`\r\n", dexterity);
        md_printf("    Strength: `bright white`%d`white`\r\n", strength);
        md_printf("      Wisdom: `bright white`%d`white`\r\n", wisdom);

        md_printf("\r\n");

        break;
      case 3:
        md_printf("`bright yellow`Wizard`white` with the following stats:\r\n");
        strength = 2;
        constitution = 4;
        wisdom = 7;
        dexterity = 1;
        
		while (stats > 0) {
			switch(rand() % 4 + 1) {
				case 1:
					wisdom += 1;
					break;
				case 2:
					dexterity += 1;
					break;
				case 3:
					strength += 1;
					break;
				default:
					constitution += 1;
					break;
			}
			stats--;
		}

        md_printf("Constitution: `bright white`%d`white`\r\n", constitution);
        md_printf("      Wisdom: `bright white`%d`white`\r\n", wisdom);
        md_printf("   Dexterity: `bright white`%d`white`\r\n", dexterity);
        md_printf("    Strength: `bright white`%d`white`\r\n", strength);

        md_printf("\r\n");

        break;
      }

      md_printf("Does that sound about right (`bright white`Y`white`/`bright "
                "white`N`white`) ");
      ch = md_get_answer("YyNn\r");

      if (ch == 'y' || ch == 'Y' || ch == '\r') {
        done = 1;
      }
    }
    
    snprintf(newsitem, 256, "%s joins the realm!", info.name);
    add_news_item(newsitem);
    
    info.userClass = userClass;
    info.wisdom = wisdom;
    info.constitution = constitution;
    info.dexterity = dexterity;
    info.strength = strength;
    info.luck = 0;
    info.charm = 0;
    info.level = 1;
    info.health = 10 * constitution * info.level;
    info.experience = 0;
    info.fights_left = ini_max_fights;
    info.last_played = time(NULL);
    info.gold = 0;
    info.weaponvalue = 0;
    info.armorvalue = 0;
    info.goldInBank = 0;
    info.bribeAmount = 0;
    info.stayingAtInn = 0;
    info.challengedMasterToday = 0;
    info.learnedSkill1 = 0;
    info.learnedSkill2 = 0;
    info.learnedSkill3 = 0;
    info.skillPoints = 3;
    info.flirtedWithAbbey = 0;
    info.listenedToOldMan = 0;
    info.gems = 0;
    info.enchant = 0;
    info.potions = 3;
    info.treasuremap = 0;
    add_player_idx();
    player_idx = get_player_idx();
    save_player();
  } else {
    ptr = localtime(&info.last_played);
    memcpy(&last_tm, ptr, sizeof(struct tm));
    timenow = time(NULL);
    ptr = localtime(&timenow);
    memcpy(&today_tm, ptr, sizeof(struct tm));

    if (today_tm.tm_yday != last_tm.tm_yday ||
        today_tm.tm_year != last_tm.tm_year) {
      // new day, start rested with max health
      info.health = max_health();
      info.fights_left = ini_max_fights;
      info.challengedMasterToday = 0;
      info.skillPoints = 3;
      info.flirtedWithAbbey = 0;
      info.listenedToOldMan = 0;
      info.stayingAtInn = 0;
      info.bribeAmount = 0;
      info.last_played = timenow;
      save_player();
    }
  }
  if (info.health <= 0) {
    md_clr_scr();
    md_sendfile("dead.ans", FALSE);
    md_printf("Press any key to continue...");
    md_getc();
    return;
  }
  md_clr_scr();
  daily_news();
  md_clr_scr();
  read_mail();

  done = 0;
  while (!done) {
    md_clr_scr();
    md_sendfile("townsquare.ans", FALSE);
    md_set_cursor(21, 1);
    md_printf("`bright white`Your Health: ");
    print_bar(info.health, max_health(), "bright red");
    md_set_cursor(22, 1);
    md_printf("`bright white` Experience: ");
    print_bar(info.experience, (int)pow(info.level, 2) * 1000, "bright green");
    if (get_unassigned_stat_points() > 0) {
      md_set_cursor(22, 44);
      md_printf("`bright yellow`Unassigned stat "
                "points!`white`");
    }
    md_set_cursor(23, 1);
    md_printf(
        "`white`What is your command? (`bright white`E`white`,`bright "
        "white`K`white`,`bright white`W`white`,`bright white`A`white`,`bright "
        "white`H`white`,`bright white`V`white`,`bright white`I`white`,`bright "
        "white`T`white`,`bright white`B`white`,`bright white`L`white`,`bright "
        "white`M`white`,`bright white`D`white`,`bright white`N`white`,`bright "
        "white`S`white`,`bright white`G`white`,`bright red`Q`white`) ");

    ch = md_get_answer("EeKkWwAaHhVvIiTtBbLlMmDdNnGgQqSsOo");

    switch (tolower(ch)) {
    case 's':
      enchantment_store();
      break;
    case 'n':
      make_announcement();
      break;
    case 'd':
      daily_news();
      break;
    case 'm':
      write_mail();
      break;
    case 'l':
      list_other_players();
      break;
    case 'b':
      bank();
      break;
    case 't':
      training_hall();
      break;
    case 'i':
      inn();
      break;
    case 'k':
      kill_other_players();
      break;
    case 'v':
      view_your_stats();
      break;
    case 'e':
      enter_the_woods();
      break;
    case 'h':
      healers();
      break;
    case 'w':
      weapon_store();
      break;
    case 'a':
      armor_store();
      break;
    case 'q':
      done = 1;
      break;
    case 'g':
      guild_hall();
      break;
    case 'o':
      other_places_menu();
      break;
    }
  }
}

void door_quit(void) {
  if (full == 1) {
    perform_maintenance();
  }

  build_bbs_scorefile();
  
  if (unlink("inuse.flg") != 0) {
    perror("unlink ");
  }
}

int main(int argc, char **argv) {
  char ch;
  int done = 0;
  FILE *fptr, *fptr2;
  struct stat s;
  int inuse = 0;
  int newgameid;
  int i;
  char message[256];
  int socket;
  ibbsmsg_t msg;
  InterBBSInfo.myNode = (tOtherNode *)malloc(sizeof(tOtherNode));
  if (InterBBSInfo.myNode == NULL) {
    fprintf(stderr, "Out of memory!\n");
    exit(-1);
  }

  if (argc < 2) {
    fprintf(stderr, "usage:\n%s (DROPFILE [SOCKET] [/full] | (maintenance | /reset GAMEID | /add NODENO, BBS NAME | /del NODENO))\n", argv[0]);
    return 0;
  }

  log_path = NULL;
  bad_path = NULL;
  delete_bad = 0;

  if (ini_parse("fh.ini", handler, NULL) < 0) {
    fprintf(stderr, "Unable to load fh.ini\n");
  }
  if (interBBSMode == 1) {
    IBSetLogger(dolog);
    if (IBReadConfig(&InterBBSInfo, BBSCFG) != eSuccess) {
      interBBSMode = 0;
    }

    if (stat("interbbs.db3", &s) != 0) {
      fprintf(stderr,
              "Unable to find interbbs.db3, have you run reset.sh?\nAre you "
              "running from the For Honour directory?\n");
      dolog("Unable to find interbbs.db3, have you run reset.sh? Are you "
              "running from the For Honour directory?");
      exit(-1);
    }
  }

  if (stat("inuse.flg", &s) == 0) {
    inuse = 1;
  }

  if (argc > 1 && strcasecmp(argv[1], "maintenance") == 0) {
    if (inuse == 1) {
      fprintf(stderr, "Game currently in use.\n");
      return 2;
    } else {
      fptr = fopen("inuse.flg", "w");
      if (!fptr) {
        fprintf(stderr, "Unable to create inuse.flg, Check permissions!\n");
        return -1;
      }
      fputs("INUSE!", fptr);
      fclose(fptr);
      perform_maintenance();
      if (unlink("inuse.flg") != 0) {
        perror("unlink ");
      }
      return 0;
    }
  }

  	if (argc > 2 && (strcasecmp(argv[1], "-ADD") == 0 || strcasecmp(argv[1], "/ADD") == 0))
	{
		printf("Adding Link %d (%s)\n", atoi(argv[2]), argv[3]);

		memset(&msg, 0, sizeof(ibbsmsg_t));
		msg.type = 5;
		msg.from = InterBBSInfo.myNode->nodeNumber;
		sprintf(msg.player_name, "%s", argv[3]);
		msg.score = 0;
        msg.experience = atoi(argv[2]);
		msg.created = time(NULL);
		msg.fights_per_day = ini_max_fights;
		msg2ne(&msg);
		IBSendAll(&InterBBSInfo, &msg, sizeof(ibbsmsg_t));
		remove_link(atoi(argv[2]));
		add_link(atoi(argv[2]), argv[3]);
		return 0;
	}

	if (argc > 1 && (strcasecmp(argv[1], "-DEL") == 0 || strcasecmp(argv[1], "/DEL") == 0))
	{
		printf("Removing Link %d\n", atoi(argv[2]));
		memset(&msg, 0, sizeof(ibbsmsg_t));

		msg.type = 5;
		msg.from = InterBBSInfo.myNode->nodeNumber;
		msg.score = 1;
		msg.experience = atoi(argv[2]);
		msg.fights_per_day = ini_max_fights;
		msg2ne(&msg);
		IBSendAll(&InterBBSInfo, &msg, sizeof(ibbsmsg_t));
		remove_link(atoi(argv[2]));
		return 0;
	}


  if (argc > 2 && (strcasecmp(argv[1], "-RESET") == 0 ||
                   strcasecmp(argv[1], "/RESET") == 0)) {
    memset(&msg, 0, sizeof(ibbsmsg_t));
    newgameid = strtoul(argv[2], NULL, 10);

    if (newgameid < 1) {
      fprintf(stderr, "Invalid game id!\n");
      return -1;
    }

    msg.type = 7;
    msg.from = InterBBSInfo.myNode->nodeNumber;
    msg.fights_per_day = ini_max_fights;
    sprintf(msg.player_name, "%d", newgameid);
    msg.created = time(NULL);
    msg2ne(&msg);
    IBSendAll(&InterBBSInfo, &msg, sizeof(ibbsmsg_t));

    fptr = fopen(BBSCFG, "r");
    if (!fptr) {
      return -1;
    }

    fptr2 = fopen(BBSCFG ".BAK", "w");
    if (!fptr2) {
      fclose(fptr);
      return -1;
    }
    fgets(message, 256, fptr);
    while (!feof(fptr)) {
      fputs(message, fptr2);
      fgets(message, 256, fptr);
    }
    fclose(fptr2);
    fclose(fptr);
    fptr = fopen(BBSCFG ".BAK", "r");
    if (!fptr) {
      return -1;
    }

    fptr2 = fopen(BBSCFG, "w");
    if (!fptr2) {
      return -1;
    }

    fgets(message, 256, fptr);
    while (!feof(fptr)) {
      if (strncasecmp(message, "GameID", 6) == 0) {
        fprintf(fptr2, "GameID %d\n", newgameid);
      } else {
        fputs(message, fptr2);
      }
      fgets(message, 256, fptr);
    }
    fclose(fptr2);
    fclose(fptr);
    unlink(BBSCFG ".BAK");

    system("./reset.sh");

    return 0;
  }

  for (i = 2; i < argc; i++) {
    if (strcasecmp(argv[i], "/full") == 0 ||
        strcasecmp(argv[i], "-full") == 0) {
      full = 1;
    }
  }

  if (argc > 2) {
    if (argv[2][0] >= '0' && argv[2][0] <= '9') {
      socket = strtol(argv[2], NULL, 10);
    } else {
      socket = -1;
    }
  } else {
    socket = -1;
  }

  md_init(argv[1], socket);

  if (stat("inuse.flg", &s) == 0) {
    md_clr_scr();
    if (stat("inuse.ans", &s) == 0) {
      md_sendfile("inuse.ans", FALSE);
    } else {
      md_printf("\r\nSorry, the game is currently in use. Please try again "
        "later\r\n" );
    }
    md_printf("Press any key to continue...");
    md_getc();
    md_exit(0);
  } else {
    fptr = fopen("inuse.flg", "w");
    if (!fptr) {
      fprintf(stderr, "Unable to open inuse.flg for writing!\n");
      md_exit(0);
    }

    fprintf(fptr, "The game is currently in use!\n");
    fclose(fptr);
  }

  atexit(door_quit);

  load_other_places();

  md_clr_scr();
  md_sendfile("forhonour.ans", FALSE);
  md_printf("`white`Press any key to continue...");
  md_getc();
  srand(time(NULL));
  while (!done) {
    md_clr_scr();
    md_sendfile("gamemenu.ans", FALSE);
    md_printf("\r\n`white`What is your command? (`bright white`P`white`,`bright "
              "white`R`white`,`bright white`L`white`,`bright white`V`white`,`bright red`Q`white`) ");
    ch = md_get_answer("PpRrLlVvQq\r");
    switch (tolower(ch)) {
    case '\r':
    case 'p':
      play_game();
      done = 1;
      break;
    case 'r':
      daily_news();
      break;
    case 'v':
      md_clr_scr();
      md_printf("`bright red`For Honour `bright white`v%d.%d-%s `bright "
                "red`Copyright (c) 2015-2021; Andrew Pamment`white`\r\n",
                VERSION_MAJOR, VERSION_MINOR, VERSION_TYPE);
      md_printf("--------------------------------------------------------------"
                "------\r\n");
      other_places_versions();
      md_printf("\r\nPress any key to continue...");
      md_getc();
      break;
    case 'l':
      md_clr_scr();
      list_other_players();
      break;
    case 'q':
      done = 1;
    }
  }
  md_exit(0);
}
